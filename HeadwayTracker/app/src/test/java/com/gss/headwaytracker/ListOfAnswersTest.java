package com.gss.headwaytracker;

import com.gss.database.model.TextMetricData;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ListOfAnswersTest {
    private TextMetricData data = new TextMetricData();

    @Test
    public void generateListFromString() {
        data.setValueString("{\"0\":\"Answer 1\",\"1\":\"Answer 2\"}");

        List<String> expected = new ArrayList<>(Arrays.asList("Answer 1", "Answer 2"));
        List<String> actual = data.getValue();

        assertEquals(expected, actual);
    }

    @Test
    public void generateStringFromList() {
        List<String> list = new ArrayList<>(Arrays.asList("Answer 1", "Answer 2"));
        data.setValue(list);

        String expected = "{\"0\":\"Answer 1\",\"1\":\"Answer 2\"}";
        String actual = data.getValueString();

        assertEquals(expected, actual);
    }
}
