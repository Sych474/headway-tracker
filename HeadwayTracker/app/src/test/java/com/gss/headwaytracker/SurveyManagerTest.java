package com.gss.headwaytracker;

import com.gss.database.model.Aspect;
import com.gss.database.model.NumberMetric;
import com.gss.database.model.TextMetric;
import com.gss.database.model.TimeMetric;
import com.gss.database.repositories.INumberMetricsRepository;
import com.gss.database.repositories.ITextMetricsRepository;
import com.gss.database.repositories.ITimeMetricsRepository;
import com.gss.headwaytracker.survey.SurveyStatusType;
import com.gss.headwaytracker.survey.surveyManager.SurveyManager;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SurveyManagerTest {

    private Aspect aspect_1 = new Aspect("aspect_1",true);
    private Aspect aspect_2 = new Aspect("aspect_2",true);
    private Aspect aspect_3 = new Aspect("aspect_3",true); // empty aspect
    private Aspect aspect_4 = new Aspect("aspect_4",true); // empty aspect

    private TextMetric textMetric = new TextMetric("text_metric_1", "question", 3, aspect_2, true);

    private NumberMetric ratingMetric = new NumberMetric("rating_metric_1", "question", 1, aspect_1, true, true);
    private NumberMetric quantitativeMetric = new NumberMetric("rating_metric_2", "question", 1, aspect_2, true, false);

    private TimeMetric intervalTimeMetric = new TimeMetric("time_metric_1", "question", true, aspect_1, true);
    private TimeMetric timeMetric = new TimeMetric("time_metric_1", "question", false, aspect_1, true);

    @Before
    public void prepareMocks() throws SQLException {
        ITextMetricsRepository textMetricsRepository = mock(ITextMetricsRepository.class);
        when(textMetricsRepository.getMetricsForAspect(aspect_1, false)).thenReturn(new ArrayList<TextMetric>());
        when(textMetricsRepository.getMetricsForAspect(aspect_2, false)).thenReturn(new ArrayList<>(Collections.singletonList(textMetric)));

        ITimeMetricsRepository timeMetricsRepository = mock(ITimeMetricsRepository.class);
        when(timeMetricsRepository.getMetricsForAspect(aspect_1, false)).thenReturn(new ArrayList<>(Arrays.asList(timeMetric, intervalTimeMetric)));
        when(timeMetricsRepository.getMetricsForAspect(aspect_2, false)).thenReturn(new ArrayList<TimeMetric>());

        INumberMetricsRepository numberMetricsRepository = mock(INumberMetricsRepository.class);
        try {
            when(numberMetricsRepository.getQuantitativeMetrics(aspect_1, false)).thenReturn(new ArrayList<NumberMetric>());
            when(numberMetricsRepository.getQuantitativeMetrics(aspect_2, false)).thenReturn(new ArrayList<>(Collections.singletonList(quantitativeMetric)));
            when(numberMetricsRepository.getRatingMetrics(aspect_1, false)).thenReturn(new ArrayList<>(Collections.singletonList(ratingMetric)));
            when(numberMetricsRepository.getRatingMetrics(aspect_2, false)).thenReturn(new ArrayList<NumberMetric>());
        } catch (SQLException e) {
            fail();
        }

        SurveyManager.setRepositories(textMetricsRepository, numberMetricsRepository, timeMetricsRepository);
    }

    @Test
    public void initFailTest() {
        //arrange
        List<Aspect> aspects = new ArrayList<>(Arrays.asList(aspect_3, aspect_4));

        //act
        boolean result = SurveyManager.init(aspects);

        //assert
        assertFalse(result);
    }

    @Test
    public void initCorrectTest() {
        //arrange
        List<Aspect> aspects = new ArrayList<>(Arrays.asList(aspect_1, aspect_2));

        //act
        boolean result = SurveyManager.init(aspects);

        //assert
        assertTrue(result);
    }

    @Test
    public void doStepTest() {
        //arrange
        List<Aspect> aspects = new ArrayList<>(Arrays.asList(aspect_1, aspect_2));

        //act
        // expected 4 steps for metrics and one final step
        boolean result = SurveyManager.init(aspects);
        SurveyStatusType status_1 = SurveyManager.getCurrentStatusType();
        Aspect resAspect_1 = SurveyManager.getCurrentAspect();
        SurveyStatusType step_status_1 = SurveyManager.doStep();
        SurveyStatusType status_2 = SurveyManager.getCurrentStatusType();
        Aspect resAspect_2 = SurveyManager.getCurrentAspect();
        SurveyStatusType step_status_2 = SurveyManager.doStep();
        SurveyStatusType status_3 = SurveyManager.getCurrentStatusType();
        Aspect resAspect_3 = SurveyManager.getCurrentAspect();
        SurveyStatusType step_status_3 = SurveyManager.doStep();
        SurveyStatusType status_4 = SurveyManager.getCurrentStatusType();
        Aspect resAspect_4 = SurveyManager.getCurrentAspect();
        SurveyStatusType step_status_4 = SurveyManager.doStep();
        SurveyStatusType status_5 = SurveyManager.getCurrentStatusType();
        Aspect resAspect_5 = SurveyManager.getCurrentAspect();
        SurveyStatusType step_status_5 = SurveyManager.doStep();

        //assert
        assertTrue(result);
        assertEquals(SurveyStatusType.TIME_METRICS, status_1);
        assertEquals(SurveyStatusType.RATING_METRICS, status_2);
        assertEquals(SurveyStatusType.TEXT_METRICS, status_3);
        assertEquals(SurveyStatusType.QUANTITATIVE_METRICS, status_4);
        assertEquals(SurveyStatusType.REPORT, status_5);

        assertEquals(SurveyStatusType.RATING_METRICS, step_status_1);
        assertEquals(SurveyStatusType.TEXT_METRICS, step_status_2);
        assertEquals(SurveyStatusType.QUANTITATIVE_METRICS, step_status_3);
        assertEquals(SurveyStatusType.REPORT, step_status_4);
        assertNull(step_status_5);

        assertAspects(aspect_1, resAspect_1);
        assertAspects(aspect_1, resAspect_2);
        assertAspects(aspect_2, resAspect_3);
        assertAspects(aspect_2, resAspect_4);
        assertNull(resAspect_5);
    }

    @Test
    public void undoStepTest() {
        //arrange
        List<Aspect> aspects = new ArrayList<>(Arrays.asList(aspect_1, aspect_2));

        //act
        // expected 4 steps for metrics and one final step
        boolean result = SurveyManager.init(aspects);
        SurveyManager.doStep();
        SurveyManager.doStep();
        SurveyManager.doStep();
        SurveyManager.doStep();
        SurveyStatusType status_1 = SurveyManager.getCurrentStatusType();
        Aspect resAspect_1 = SurveyManager.getCurrentAspect();
        SurveyStatusType step_status_1 = SurveyManager.undoStep();
        SurveyStatusType status_2 = SurveyManager.getCurrentStatusType();
        Aspect resAspect_2 = SurveyManager.getCurrentAspect();
        SurveyStatusType step_status_2 = SurveyManager.undoStep();
        SurveyStatusType status_3 = SurveyManager.getCurrentStatusType();
        Aspect resAspect_3 = SurveyManager.getCurrentAspect();
        SurveyStatusType step_status_3 = SurveyManager.undoStep();
        SurveyStatusType status_4 = SurveyManager.getCurrentStatusType();
        Aspect resAspect_4 = SurveyManager.getCurrentAspect();
        SurveyStatusType step_status_4 = SurveyManager.undoStep();
        SurveyStatusType status_5 = SurveyManager.getCurrentStatusType();
        Aspect resAspect_5 = SurveyManager.getCurrentAspect();
        SurveyStatusType step_status_5 = SurveyManager.undoStep();

        //assert
        assertTrue(result);
        assertEquals(SurveyStatusType.REPORT, status_1);
        assertEquals(SurveyStatusType.QUANTITATIVE_METRICS, status_2);
        assertEquals(SurveyStatusType.TEXT_METRICS, status_3);
        assertEquals(SurveyStatusType.RATING_METRICS, status_4);
        assertEquals(SurveyStatusType.TIME_METRICS, status_5);

        assertEquals(SurveyStatusType.QUANTITATIVE_METRICS, step_status_1);
        assertEquals(SurveyStatusType.TEXT_METRICS, step_status_2);
        assertEquals(SurveyStatusType.RATING_METRICS, step_status_3);
        assertEquals(SurveyStatusType.TIME_METRICS, step_status_4);
        assertNull(step_status_5);

        assertNull(resAspect_1);
        assertAspects(aspect_2, resAspect_2);
        assertAspects(aspect_2, resAspect_3);
        assertAspects(aspect_1, resAspect_4);
        assertAspects(aspect_1, resAspect_5);
    }

    private void assertAspects(Aspect expected, Aspect actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.isActive(), actual.isActive());
    }
}
