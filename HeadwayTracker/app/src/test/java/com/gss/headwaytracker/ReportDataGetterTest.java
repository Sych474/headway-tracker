package com.gss.headwaytracker;

import com.gss.database.model.Aspect;
import com.gss.database.model.NumberMetric;
import com.gss.database.model.NumberMetricData;
import com.gss.database.model.TimeMetric;
import com.gss.database.model.TimeMetricData;
import com.gss.database.repositories.IAspectsRepository;
import com.gss.database.repositories.INumberMetricsRepository;
import com.gss.database.repositories.ITimeMetricsRepository;
import com.gss.headwaytracker.reports.DataPair;
import com.gss.headwaytracker.reports.ReportDataGetter;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ReportDataGetterTest {

    private Aspect aspect = new Aspect("aspect_1",true);

    private NumberMetric ratingMetric = new NumberMetric("rating_metric_1", "question", 5, aspect, true, true);
    private NumberMetric quantitativeMetric = new NumberMetric("rating_metric_2", "question", 5, aspect, true, false);

    private TimeMetric timeMetric = new TimeMetric("time_metric", "question", false, aspect, true);
    private TimeMetric intervalMetric = new TimeMetric("time_metric_2", "question", true, aspect, true);

    private INumberMetricsRepository numberMetricsRepository = null;
    private ITimeMetricsRepository timeMetricsRepository = null;
    private IAspectsRepository aspectsRepository = null;

    @Test(expected = Exception.class)
    public void getDataForRatingMetricExceptionTest() throws Exception {
        //arrange
        ReportDataGetter dataGetter = new ReportDataGetter(timeMetricsRepository, numberMetricsRepository, aspectsRepository);

        //act and assert
        dataGetter.getDataForRatingMetric(quantitativeMetric, new Date(), new Date());
    }

    @Test
    public void getDataForRatingMetricNoDataTest() throws Exception {
        //arrange
        Date startDate = new Date(2000, 0, 10, 1, 1, 1);
        Date endDate = new Date(2000, 0, 14, 1, 1, 1);
        INumberMetricsRepository numberMetricsRepository = mock(INumberMetricsRepository.class);
        when(numberMetricsRepository.getDataForMetrics(ratingMetric, startDate, endDate)).thenReturn(new ArrayList<NumberMetricData>());

        ReportDataGetter dataGetter = new ReportDataGetter(timeMetricsRepository, numberMetricsRepository, aspectsRepository);

        //act
        List<DataPair<Integer>> result = dataGetter.getDataForRatingMetric(ratingMetric, startDate, endDate);

        //assert
        assertEquals(5, result.size());
        assertEquals((Integer) 0, result.get(0).getValue());
        assertEquals((Integer) 0, result.get(1).getValue());
        assertEquals((Integer) 0, result.get(2).getValue());
        assertEquals((Integer) 0, result.get(3).getValue());
        assertEquals((Integer) 0, result.get(4).getValue());

        assertEquals("01-10", result.get(0).getDate());
        assertEquals("01-11", result.get(1).getDate());
        assertEquals("01-12", result.get(2).getDate());
        assertEquals("01-13", result.get(3).getDate());
        assertEquals("01-14", result.get(4).getDate());
    }

    @Test
    public void getDataForRatingMetricWithDataTest() throws Exception {
        //arrange
        Date startDate = new Date(2000, 0, 10, 1, 1, 1);
        Date endDate = new Date(2000, 0, 14, 1, 1, 1);
        NumberMetricData[] data = new NumberMetricData[] {
                new NumberMetricData(1, new Date(2000, 0, 11, 0, 0, 0), ratingMetric),
                new NumberMetricData(2, new Date(2000, 0, 13, 0, 0, 0), ratingMetric),
        };
        numberMetricsRepository = mock(INumberMetricsRepository.class);
        when(numberMetricsRepository.getDataForMetrics(ratingMetric, startDate, endDate)).thenReturn(
                new ArrayList<>(Arrays.asList(data)));

        ReportDataGetter dataGetter = new ReportDataGetter(timeMetricsRepository, numberMetricsRepository, aspectsRepository);

        //act
        List<DataPair<Integer>> result = dataGetter.getDataForRatingMetric(ratingMetric, startDate, endDate);

        //assert
        assertEquals(5, result.size());
        assertEquals((Integer) 0, result.get(0).getValue());
        assertEquals((Integer) 1, result.get(1).getValue());
        assertEquals((Integer) 0, result.get(2).getValue());
        assertEquals((Integer) 2, result.get(3).getValue());
        assertEquals((Integer) 0, result.get(4).getValue());

        assertEquals("01-10", result.get(0).getDate());
        assertEquals("01-11", result.get(1).getDate());
        assertEquals("01-12", result.get(2).getDate());
        assertEquals("01-13", result.get(3).getDate());
        assertEquals("01-14", result.get(4).getDate());
    }

    @Test
    public void getDataForQuantitativeMetricNoDataTest() throws Exception {
        //arrange
        Date startDate = new Date(2000, 0, 10, 1, 1, 1);
        Date endDate = new Date(2000, 0, 14, 1, 1, 1);
        INumberMetricsRepository numberMetricsRepository = mock(INumberMetricsRepository.class);
        when(numberMetricsRepository.getDataForMetrics(quantitativeMetric, startDate, endDate)).thenReturn(new ArrayList<NumberMetricData>());

        ReportDataGetter dataGetter = new ReportDataGetter(timeMetricsRepository, numberMetricsRepository, aspectsRepository);

        //act
        List<DataPair<Integer>> result = dataGetter.getDataForQuantitativeMetric(quantitativeMetric, startDate, endDate);

        //assert
        assertEquals(5, result.size());
        assertEquals((Integer) 0, result.get(0).getValue());
        assertEquals((Integer) 0, result.get(1).getValue());
        assertEquals((Integer) 0, result.get(2).getValue());
        assertEquals((Integer) 0, result.get(3).getValue());
        assertEquals((Integer) 0, result.get(4).getValue());

        assertEquals("01-10", result.get(0).getDate());
        assertEquals("01-11", result.get(1).getDate());
        assertEquals("01-12", result.get(2).getDate());
        assertEquals("01-13", result.get(3).getDate());
        assertEquals("01-14", result.get(4).getDate());
    }

    @Test
    public void getDataForQuantitativeMetricWithDataTest() throws Exception {
        //arrange
        Date startDate = new Date(2000, 0, 10, 1, 1, 1);
        Date endDate = new Date(2000, 0, 14, 1, 1, 1);
        NumberMetricData[] data = new NumberMetricData[] {
                new NumberMetricData(1, new Date(2000, 0, 11, 0, 0, 0), quantitativeMetric),
                new NumberMetricData(2, new Date(2000, 0, 13, 0, 0, 0), quantitativeMetric),
        };
        numberMetricsRepository = mock(INumberMetricsRepository.class);
        when(numberMetricsRepository.getDataForMetrics(quantitativeMetric, startDate, endDate)).thenReturn(
                new ArrayList<>(Arrays.asList(data)));

        ReportDataGetter dataGetter = new ReportDataGetter(timeMetricsRepository, numberMetricsRepository, aspectsRepository);

        //act
        List<DataPair<Integer>> result = dataGetter.getDataForQuantitativeMetric(quantitativeMetric, startDate, endDate);

        //assert
        assertEquals(5, result.size());
        assertEquals((Integer) 0, result.get(0).getValue());
        assertEquals((Integer) 1, result.get(1).getValue());
        assertEquals((Integer) 0, result.get(2).getValue());
        assertEquals((Integer) 2, result.get(3).getValue());
        assertEquals((Integer) 0, result.get(4).getValue());

        assertEquals("01-10", result.get(0).getDate());
        assertEquals("01-11", result.get(1).getDate());
        assertEquals("01-12", result.get(2).getDate());
        assertEquals("01-13", result.get(3).getDate());
        assertEquals("01-14", result.get(4).getDate());
    }

    @Test
    public void getDataForTimeMetricNoDataTest() throws Exception {
        //arrange
        Date startDate = new Date(2000, 0, 10, 1, 1, 1);
        Date endDate = new Date(2000, 0, 14, 1, 1, 1);
        timeMetricsRepository = mock(ITimeMetricsRepository.class);
        when(timeMetricsRepository.getDataForMetrics(timeMetric, startDate, endDate)).thenReturn(new ArrayList<TimeMetricData>());

        ReportDataGetter dataGetter = new ReportDataGetter(timeMetricsRepository, numberMetricsRepository, aspectsRepository);

        //act
        List<DataPair<Integer>> result = dataGetter.getDataForTimeMetric(timeMetric, startDate, endDate);

        //assert
        assertEquals(5, result.size());
        assertEquals((Integer) 0, result.get(0).getValue());
        assertEquals((Integer) 0, result.get(1).getValue());
        assertEquals((Integer) 0, result.get(2).getValue());
        assertEquals((Integer) 0, result.get(3).getValue());
        assertEquals((Integer) 0, result.get(4).getValue());

        assertEquals("01-10", result.get(0).getDate());
        assertEquals("01-11", result.get(1).getDate());
        assertEquals("01-12", result.get(2).getDate());
        assertEquals("01-13", result.get(3).getDate());
        assertEquals("01-14", result.get(4).getDate());
    }

    @Test
    public void getDataForTimeMetricWithDataTest() throws Exception {
        //arrange
        Date startDate = new Date(2000, 0, 10, 1, 1, 1);
        Date endDate = new Date(2000, 0, 14, 1, 1, 1);
        TimeMetricData[] data = new TimeMetricData[] {
                new TimeMetricData(new Date(1000 * 60), null,  new Date(2000, 0, 11, 0, 0, 0), timeMetric),
                new TimeMetricData(new Date(2000 * 60), null,  new Date(2000, 0, 13, 0, 0, 0), timeMetric),
        };
        timeMetricsRepository = mock(ITimeMetricsRepository.class);
        when(timeMetricsRepository.getDataForMetrics(timeMetric, startDate, endDate)).thenReturn(
                new ArrayList<>(Arrays.asList(data)));

        ReportDataGetter dataGetter = new ReportDataGetter(timeMetricsRepository, numberMetricsRepository, aspectsRepository);

        //act
        List<DataPair<Integer>> result = dataGetter.getDataForTimeMetric(timeMetric, startDate, endDate);

        //assert
        assertEquals(5, result.size());
        assertEquals((Integer) 0, result.get(0).getValue());
        assertEquals((Integer) 1, result.get(1).getValue());
        assertEquals((Integer) 0, result.get(2).getValue());
        assertEquals((Integer) 2, result.get(3).getValue());
        assertEquals((Integer) 0, result.get(4).getValue());

        assertEquals("01-10", result.get(0).getDate());
        assertEquals("01-11", result.get(1).getDate());
        assertEquals("01-12", result.get(2).getDate());
        assertEquals("01-13", result.get(3).getDate());
        assertEquals("01-14", result.get(4).getDate());
    }

    @Test
    public void getAverageDataForRatingMetricTest() throws Exception {
        //arrange
        Date startDate = new Date(2000, 0, 10, 1, 1, 1);
        Date endDate = new Date(2000, 0, 25, 1, 1, 1);
        NumberMetricData[] data = new NumberMetricData[] {
                new NumberMetricData(1, new Date(2000, 0, 10, 0, 0, 0), ratingMetric),

                new NumberMetricData(2, new Date(2000, 0, 13, 0, 0, 0), ratingMetric),
                new NumberMetricData(2, new Date(2000, 0, 14, 0, 0, 0), ratingMetric),
                new NumberMetricData(2, new Date(2000, 0, 15, 0, 0, 0), ratingMetric),

                new NumberMetricData(3, new Date(2000, 0, 16, 0, 0, 0), ratingMetric),
                new NumberMetricData(3, new Date(2000, 0, 17, 0, 0, 0), ratingMetric),

                new NumberMetricData(5, new Date(2000, 0, 18, 0, 0, 0), ratingMetric),
                new NumberMetricData(5, new Date(2000, 0, 19, 0, 0, 0), ratingMetric),
        };
        numberMetricsRepository = mock(INumberMetricsRepository.class);
        when(numberMetricsRepository.getDataForMetrics(ratingMetric, startDate, endDate)).thenReturn(
                new ArrayList<>(Arrays.asList(data)));

        ReportDataGetter dataGetter = new ReportDataGetter(timeMetricsRepository, numberMetricsRepository, aspectsRepository);

        //act
        List<DataPair<Integer>> result = dataGetter.getAverageDataForRatingMetric(ratingMetric, startDate, endDate);

        //assert
        assertEquals(4, result.size());
        assertEquals((Integer) 1, result.get(0).getValue());
        assertEquals((Integer) 3, result.get(1).getValue());
        assertEquals((Integer) 2, result.get(2).getValue());
        assertEquals((Integer) 2, result.get(3).getValue());

        assertEquals("1", result.get(0).getDate());
        assertEquals("2", result.get(1).getDate());
        assertEquals("3", result.get(2).getDate());
        assertEquals("5", result.get(3).getDate());
    }

    @Test
    public void getAverageTimeForAspectsSimpleTest() throws Exception {
        //arrange
        Aspect aspect_1 = new Aspect("aspect_1",true);

        TimeMetric metric11 = new TimeMetric("tm21", "q", false, aspect_1, true);

        Date startDate = new Date(2000, 0, 10, 1, 1, 1);
        Date endDate = new Date(2000, 0, 11, 1, 1, 1);
        // 2 days

        List<Aspect> aspects = new ArrayList<>(Collections.singletonList(aspect_1));

        TimeMetric[] metrics_1 = new TimeMetric[] { metric11 };

        TimeMetricData[] data11 = new TimeMetricData[] {
                new TimeMetricData(new Date(2 * 1000 * 60), null,  new Date(2000, 0, 11, 0, 0, 0), timeMetric),
                new TimeMetricData(new Date(4 * 1000 * 60), null,  new Date(2000, 0, 13, 0, 0, 0), timeMetric),
        };

        timeMetricsRepository = mock(ITimeMetricsRepository.class);
        when(timeMetricsRepository.getMetricsForAspect(aspect_1, true)).thenReturn(
                new ArrayList<>(Arrays.asList(metrics_1)));
        when(timeMetricsRepository.getDataForMetrics(metric11, startDate, endDate)).thenReturn(
                new ArrayList<>(Arrays.asList(data11)));

        ReportDataGetter dataGetter = new ReportDataGetter(timeMetricsRepository, numberMetricsRepository, aspectsRepository);

        //act
        List<DataPair<Integer>> result = dataGetter.getAverageTimesForAspects(aspects, startDate, endDate);

        //assert
        assertEquals(1, result.size());
        assertEquals((Integer) 3, result.get(0).getValue());

        assertEquals("aspect_1", result.get(0).getDate());
    }

    @Test
    public void getAverageTimeForAspectsTest() throws Exception {
        //arrange
        Aspect aspect_1 = new Aspect("aspect_1",true);
        Aspect aspect_2 = new Aspect("aspect_2",true);

        TimeMetric metric11 = new TimeMetric("tm21", "q", false, aspect_1, true);
        TimeMetric metric21 = new TimeMetric("tm31", "q", false, aspect_2, true);
        TimeMetric metric22 = new TimeMetric("tm32", "q", true, aspect_2, true);

        Date startDate = new Date(2000, 0, 10, 1, 1, 1);
        Date endDate = new Date(2000, 0, 13, 1, 1, 1);
        // 4 days

        List<Aspect> aspects = new ArrayList<>(Arrays.asList(aspect_1, aspect_2));

        TimeMetric[] metrics_1 = new TimeMetric[] { metric11 };

        TimeMetric[] metrics_2 = new TimeMetric[] {metric21, metric22 };

        TimeMetricData[] data11 = new TimeMetricData[] {
                new TimeMetricData(new Date(4 * 1000 * 60), null,  new Date(2000, 0, 11, 0, 0, 0), timeMetric),
                new TimeMetricData(new Date(8 * 1000 * 60), null,  new Date(2000, 0, 13, 0, 0, 0), timeMetric),
        };

        TimeMetricData[] data21 = new TimeMetricData[] {
                new TimeMetricData(new Date(4 * 1000 * 60), null,  new Date(2000, 0, 11, 0, 0, 0), timeMetric),
        };

        TimeMetricData[] data22 = new TimeMetricData[] {
                new TimeMetricData(new Date(4 * 1000 * 60), null,  new Date(2000, 0, 11, 0, 0, 0), timeMetric),
                new TimeMetricData(new Date(8 * 1000 * 60), null,  new Date(2000, 0, 13, 0, 0, 0), timeMetric),
        };

        timeMetricsRepository = mock(ITimeMetricsRepository.class);
        when(timeMetricsRepository.getMetricsForAspect(aspect_1, true)).thenReturn(
                new ArrayList<>(Arrays.asList(metrics_1)));
        when(timeMetricsRepository.getMetricsForAspect(aspect_2, true)).thenReturn(
                new ArrayList<>(Arrays.asList(metrics_2)));

        when(timeMetricsRepository.getDataForMetrics(metric11, startDate, endDate)).thenReturn(
                new ArrayList<>(Arrays.asList(data11)));
        when(timeMetricsRepository.getDataForMetrics(metric21, startDate, endDate)).thenReturn(
                new ArrayList<>(Arrays.asList(data21)));
        when(timeMetricsRepository.getDataForMetrics(metric22, startDate, endDate)).thenReturn(
                new ArrayList<>(Arrays.asList(data22)));

        ReportDataGetter dataGetter = new ReportDataGetter(timeMetricsRepository, numberMetricsRepository, aspectsRepository);

        //act
        List<DataPair<Integer>> result = dataGetter.getAverageTimesForAspects(aspects, startDate, endDate);

        //assert
        assertEquals(2, result.size());
        assertEquals((Integer) 3, result.get(0).getValue());
        assertEquals((Integer) 4, result.get(1).getValue());

        assertEquals("aspect_1", result.get(0).getDate());
        assertEquals("aspect_2", result.get(1).getDate());
    }

    @Test(expected = Exception.class)
    public void getDataForNumberMetricExceptionTest() throws Exception {
        //arrange
        ReportDataGetter dataGetter = new ReportDataGetter(timeMetricsRepository, numberMetricsRepository, aspectsRepository);

        //act and assert
        dataGetter.getDataForQuantitativeMetric(ratingMetric, new Date(), new Date());
    }
}
