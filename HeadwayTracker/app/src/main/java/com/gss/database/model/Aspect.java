package com.gss.database.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

public class Aspect {

    public final static String NAME_FIELD_NAME = "name";
    public final static String IS_ACTIVE_FIELD_NAME = "is_active";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(unique = true, dataType = DataType.STRING, columnName = NAME_FIELD_NAME)
    private String name;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = IS_ACTIVE_FIELD_NAME)
    private boolean isActive;

    public Aspect() {
    }

    public Aspect(String name, boolean isActive) {
        this.name = name;
        this.isActive = isActive;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

}
