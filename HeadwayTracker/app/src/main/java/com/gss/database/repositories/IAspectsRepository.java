package com.gss.database.repositories;

import com.gss.database.model.Aspect;
import java.util.List;

/**Describes methods on aspects table*/
public interface IAspectsRepository  {

    /** Returns all aspects from database*/
    List<Aspect> getAspects(boolean onlyActive);

    /**Add or update aspect, if something go wrong return null, else - return updated aspect*/
    Aspect addOrUpdateAspect(Aspect aspect);

    /**Remove aspect for database*/
    void removeAspect(Aspect aspect);
}
