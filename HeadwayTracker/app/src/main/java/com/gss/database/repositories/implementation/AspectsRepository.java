package com.gss.database.repositories.implementation;

import com.gss.database.HelperFactory;
import com.gss.database.model.Aspect;
import com.gss.database.repositories.IAspectsRepository;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.List;

public class AspectsRepository implements IAspectsRepository {

    private RuntimeExceptionDao<Aspect, Integer> aspectsRuntimeDao;

    public AspectsRepository() {
        this.aspectsRuntimeDao = HelperFactory.getHelper().getAspectsRuntimeExceptionDao();
    }

    @Override
    public List<Aspect> getAspects(boolean onlyActive) {
        if (!onlyActive)
            return HelperFactory.getHelper().getAspectsRuntimeExceptionDao().queryForAll();
        else
            return HelperFactory.getHelper().getAspectsRuntimeExceptionDao().queryForEq(Aspect.IS_ACTIVE_FIELD_NAME, true);
    }

    @Override
    public Aspect addOrUpdateAspect(Aspect aspect) {
        Dao.CreateOrUpdateStatus status = aspectsRuntimeDao.createOrUpdate(aspect);
        if (!status.isCreated() && !status.isUpdated())
            return null;
        return aspect;
    }

    @Override
    public void removeAspect(Aspect aspect) {
        aspectsRuntimeDao.delete(aspect);
    }
}
