package com.gss.database.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TextMetricData implements IMetricData {

    public final static String VALUE_FIELD_NAME = "value";
    public final static String DATE_FIELD_NAME = "date";
    public final static String METRIC_FIELD_NAME = "metric_id";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.STRING, columnName = VALUE_FIELD_NAME)
    private String value;

    @DatabaseField(index = true, dataType = DataType.DATE, columnName = DATE_FIELD_NAME)
    private Date date;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private TextMetric metric;

    public TextMetricData() {
    }

    public TextMetricData(String value, Date date, TextMetric metric) {
        this.value = value;
        this.date = date;
        this.metric = metric;
    }

    public int getId() {
        return id;
    }


    public String getValueString() {
        return value;
    }


    public List<String> getValue() {
        return generateAnswersFromString(value);
    }


    public void setValueString(String value) {
        this.value = value;
    }


    public void setValue(List<String> value) {
        this.value = generateStringFromAnswers(value);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public TextMetric getMetric() {
        return metric;
    }

    public void setMetric(TextMetric metric) {
        this.metric = metric;
    }

    private List<String> generateAnswersFromString(String value) {
        List<String> answers = new ArrayList<>();

        try {
            JSONObject array = new JSONObject(value);

            for (int i = 0; i < array.length(); i++) {
                String str = array.getString(Integer.toString(i));
                answers.add(str);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return answers;
    }

    private String generateStringFromAnswers(List<String> answers) {
        String string = "";

        try {
            JSONObject jsonObject = new JSONObject();
            for (int i = 0; i < answers.size(); i++) {
                jsonObject.put(Integer.toString(i), answers.get(i));
            }
            string = jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return string;
    }
}
