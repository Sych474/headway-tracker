package com.gss.database.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

import java.util.Date;

public class NumberMetricData implements IMetricData {

    public final static String VALUE_FIELD_NAME = "value";
    public final static String DATE_FIELD_NAME = "date";
    public final static String METRIC_FIELD_NAME = "metric_id";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.INTEGER, columnName = VALUE_FIELD_NAME)
    private int value;

    @DatabaseField(index = true, dataType = DataType.DATE, columnName = DATE_FIELD_NAME)
    private Date date;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private NumberMetric metric;

    public NumberMetricData() {
    }

    public NumberMetricData(int value, Date date, NumberMetric metric) {
        this.value = value;
        this.date = date;
        this.metric = metric;
    }

    public int getId() {
        return id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public NumberMetric getMetric() {
        return metric;
    }

    public void setMetric(NumberMetric metric) {
        this.metric = metric;
    }
}
