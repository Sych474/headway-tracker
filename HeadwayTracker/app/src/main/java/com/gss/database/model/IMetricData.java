package com.gss.database.model;

import java.util.Date;

public interface IMetricData {
    Date getDate();
}
