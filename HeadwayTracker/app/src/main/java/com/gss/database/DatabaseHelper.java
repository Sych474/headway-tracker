package com.gss.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.gss.database.model.Aspect;
import com.gss.database.model.NumberMetric;
import com.gss.database.model.NumberMetricData;
import com.gss.database.model.TextMetric;
import com.gss.database.model.TextMetricData;
import com.gss.database.model.TimeMetric;
import com.gss.database.model.TimeMetricData;
import com.gss.headwaytracker.R;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs used by the other classes.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "HeadwayTracker.db";

    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 1;

    private RuntimeExceptionDao<Aspect, Integer> aspectsRuntimeDao = null;
    private RuntimeExceptionDao<TextMetric, Integer> textMetricsRuntimeDao = null;
    private RuntimeExceptionDao<TextMetricData, Integer> textMetricsDataRuntimeDao = null;
    private RuntimeExceptionDao<NumberMetric, Integer> numberMetricsRuntimeDao = null;
    private RuntimeExceptionDao<NumberMetricData, Integer> numberMetricsDataRuntimeDao = null;
    private RuntimeExceptionDao<TimeMetric, Integer> timeMetricsRuntimeDao = null;
    private RuntimeExceptionDao<TimeMetricData, Integer> timeMetricsDataRuntimeDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    /**
     * This is called when the database is first created. Usually you should call createTable statements here to create
     * the tables that will store your data.
     */
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, Aspect.class);
            TableUtils.createTable(connectionSource, TextMetric.class);
            TableUtils.createTable(connectionSource, TextMetricData.class);
            TableUtils.createTable(connectionSource, NumberMetric.class);
            TableUtils.createTable(connectionSource, NumberMetricData.class);
            TableUtils.createTable(connectionSource, TimeMetric.class);
            TableUtils.createTable(connectionSource, TimeMetricData.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, Aspect.class, true);
            TableUtils.dropTable(connectionSource, TextMetric.class, true);
            TableUtils.dropTable(connectionSource, TextMetricData.class,true);
            TableUtils.dropTable(connectionSource, NumberMetric.class,true);
            TableUtils.dropTable(connectionSource, NumberMetricData.class,true);
            TableUtils.dropTable(connectionSource, TimeMetric.class,true);
            TableUtils.dropTable(connectionSource, TimeMetricData.class,true);
            // after we drop the old databases, we create the new ones
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    public RuntimeExceptionDao<Aspect, Integer> getAspectsRuntimeExceptionDao() {
        if (aspectsRuntimeDao == null) {
            aspectsRuntimeDao = getRuntimeExceptionDao(Aspect.class);
        }
        return aspectsRuntimeDao;
    }

    public RuntimeExceptionDao<TextMetric, Integer> getTextMetricsRuntimeExceptionDao() {
        if (textMetricsRuntimeDao == null) {
            textMetricsRuntimeDao = getRuntimeExceptionDao(TextMetric.class);
        }
        return textMetricsRuntimeDao;
    }

    public RuntimeExceptionDao<TextMetricData, Integer> getTextMetricsDataRuntimeExceptionDao() {
        if (textMetricsDataRuntimeDao == null) {
            textMetricsDataRuntimeDao = getRuntimeExceptionDao(TextMetricData.class);
        }
        return textMetricsDataRuntimeDao;
    }

    public RuntimeExceptionDao<NumberMetric, Integer> getNumberMetricsRuntimeExceptionDao() {
        if (numberMetricsRuntimeDao == null) {
            numberMetricsRuntimeDao = getRuntimeExceptionDao(NumberMetric.class);
        }
        return numberMetricsRuntimeDao;
    }

    public RuntimeExceptionDao<NumberMetricData, Integer> getNumberMetricsDataRuntimeExceptionDao() {
        if (numberMetricsDataRuntimeDao == null) {
            numberMetricsDataRuntimeDao = getRuntimeExceptionDao(NumberMetricData.class);
        }
        return numberMetricsDataRuntimeDao;
    }

    public RuntimeExceptionDao<TimeMetric, Integer> getTimeMetricsRuntimeExceptionDao() {
        if (timeMetricsRuntimeDao == null) {
            timeMetricsRuntimeDao = getRuntimeExceptionDao(TimeMetric.class);
        }
        return timeMetricsRuntimeDao;
    }

    public RuntimeExceptionDao<TimeMetricData, Integer> getTimeMetricsDataRuntimeExceptionDao() {
        if (timeMetricsDataRuntimeDao == null) {
            timeMetricsDataRuntimeDao = getRuntimeExceptionDao(TimeMetricData.class);
        }
        return timeMetricsDataRuntimeDao;
    }

    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();
        aspectsRuntimeDao = null;
        textMetricsRuntimeDao = null;
        textMetricsDataRuntimeDao = null;
        numberMetricsRuntimeDao = null;
        numberMetricsDataRuntimeDao = null;
        timeMetricsRuntimeDao = null;
        timeMetricsDataRuntimeDao = null;
    }
}