package com.gss.database.repositories.implementation;

import com.gss.database.HelperFactory;
import com.gss.database.model.Aspect;
import com.gss.database.model.TimeMetric;
import com.gss.database.model.TimeMetricData;
import com.gss.database.repositories.ITimeMetricsRepository;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class TimeMetricsRepository implements ITimeMetricsRepository {

    private RuntimeExceptionDao<TimeMetric, Integer> timeMetricsRuntimeDao;
    private RuntimeExceptionDao<TimeMetricData, Integer> timeMetricsDataRuntimeDao;

    public TimeMetricsRepository() {
        this.timeMetricsRuntimeDao = HelperFactory.getHelper().getTimeMetricsRuntimeExceptionDao();
        this.timeMetricsDataRuntimeDao = HelperFactory.getHelper().getTimeMetricsDataRuntimeExceptionDao();
    }

    @Override
    public List<TimeMetric> getTimeSimpleMetrics(boolean onlyActive) throws SQLException {
        Where<TimeMetric, Integer> query = timeMetricsRuntimeDao.queryBuilder().where().eq(TimeMetric.IS_INTERVAL_FIELD_NAME, false);
        if (onlyActive)
            query = query.and().eq(TimeMetric.IS_ACTIVE_FIELD_NAME, true);

        return query.query();
    }

    @Override
    public List<TimeMetric> getTimeIntervalMetrics(boolean onlyActive) throws SQLException {
        Where<TimeMetric, Integer> query = timeMetricsRuntimeDao.queryBuilder().where().eq(TimeMetric.IS_INTERVAL_FIELD_NAME, true);
        if (onlyActive)
            query = query.and().eq(TimeMetric.IS_ACTIVE_FIELD_NAME, true);

        return query.query();
    }

    @Override
    public TimeMetric findMetricsByName(String name) {
        List<TimeMetric> results =  timeMetricsRuntimeDao.queryForEq(TimeMetric.NAME_FIELD_NAME, name);
        if (results.size() == 1)
            return results.get(0);
        else
            return null;
    }

    @Override
    public List<TimeMetric> getMetrics(boolean onlyActive) {
        if (!onlyActive)
            return timeMetricsRuntimeDao.queryForAll();
        else
            return timeMetricsRuntimeDao.queryForEq(TimeMetric.IS_ACTIVE_FIELD_NAME, true);
    }

    @Override
    public List<TimeMetric> getMetricsForAspect(Aspect aspect, boolean onlyActive) throws SQLException {
        Where<TimeMetric, Integer> query = timeMetricsRuntimeDao.queryBuilder().where().eq(TimeMetric.ASPECT_FIELD_NAME, aspect);
        if (onlyActive)
            query = query.and().eq(TimeMetric.IS_ACTIVE_FIELD_NAME, true);

        return query.query();
    }

    @Override
    public TimeMetric addOrUpdateMetric(TimeMetric timeMetric) {
        Dao.CreateOrUpdateStatus status = timeMetricsRuntimeDao.createOrUpdate(timeMetric);
        if (!status.isCreated() && !status.isUpdated())
            return null;
        timeMetricsRuntimeDao.refresh(timeMetric);
        return timeMetric;
    }

    @Override
    public void removeMetric(TimeMetric timeMetric) {
        timeMetricsRuntimeDao.delete(timeMetric);
    }

    @Override
    public List<TimeMetricData> getAllDataForMetrics(TimeMetric timeMetric) {
        return timeMetricsDataRuntimeDao.queryForEq(TimeMetricData.METRIC_FIELD_NAME, timeMetric);
    }

    @Override
    public List<TimeMetricData> getDataForMetrics(TimeMetric timeMetric, Date startDate, Date endDate) throws SQLException {
        return timeMetricsDataRuntimeDao.queryBuilder().where()
                .eq(TimeMetricData.METRIC_FIELD_NAME, timeMetric).and()
                .between(TimeMetricData.DATE_FIELD_NAME, startDate, endDate).query();
    }

    @Override
    public TimeMetricData addMetricData(TimeMetric timeMetric, TimeMetricData timeMetricData) {
        if (timeMetric == null)
            return null;

        timeMetricData.setMetric(timeMetric);
        int result = timeMetricsDataRuntimeDao.create(timeMetricData);
        if (result != 1)
            return null;

        timeMetricsDataRuntimeDao.refresh(timeMetricData);
        return timeMetricData;
    }

    @Override
    public void updateMetricData(TimeMetricData timeMetricData) {
        timeMetricsDataRuntimeDao.update(timeMetricData);
    }

    @Override
    public void removeMetricData(TimeMetricData timeMetricData) {
        timeMetricsDataRuntimeDao.delete(timeMetricData);
    }

    @Override
    public void removeAllMetricsData(TimeMetric metric) throws SQLException {
        DeleteBuilder<TimeMetricData, Integer> deleteBuilder  = timeMetricsDataRuntimeDao.deleteBuilder();
        deleteBuilder.where().eq(TimeMetricData.METRIC_FIELD_NAME, metric);
        deleteBuilder.delete();
    }
}
