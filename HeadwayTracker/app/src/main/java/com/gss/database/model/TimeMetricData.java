package com.gss.database.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

import java.util.Date;

public class TimeMetricData implements IMetricData {

    public final static String DATE_FIELD_NAME = "date";
    public final static String START_TIME_FIELD_NAME = "start_time";
    public final static String END_TIME_FIELD_NAME = "end_time";
    public final static String METRIC_FIELD_NAME = "metric_id";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.DATE, columnName = START_TIME_FIELD_NAME)
    private Date startTime;

    @DatabaseField(dataType = DataType.DATE, columnName = END_TIME_FIELD_NAME)
    private Date endTime;

    @DatabaseField(index = true, dataType = DataType.DATE, columnName = DATE_FIELD_NAME)
    private Date date;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private TimeMetric metric;

    public TimeMetricData() {
    }

    public TimeMetricData(Date startTime, Date endTime, Date date, TimeMetric metric) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.date = date;
        this.metric = metric;
    }

    public int getId() {
        return id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public TimeMetric getMetric() {
        return metric;
    }

    public void setMetric(TimeMetric metric) {
        this.metric = metric;
    }

    public long getIntervalInMiliseconds(){
        if (this.endTime != null){
            return endTime.getTime() - startTime.getTime();
        }
        return startTime.getTime();
    }
}
