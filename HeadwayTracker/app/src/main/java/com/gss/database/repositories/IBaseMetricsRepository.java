package com.gss.database.repositories;

import com.gss.database.model.Aspect;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**Describes basic the methods on any table of metrics*/
public interface IBaseMetricsRepository<Metric, MetricData> {

    /**Find metrics by name, if can't find - return null*/
    Metric findMetricsByName(String name);

    /**Returns all metrics*/
    List<Metric> getMetrics(boolean onlyActive);

    /**Returns all metrics for aspect*/
    List<Metric> getMetricsForAspect(Aspect aspect, boolean onlyActive) throws SQLException;

    /**Add or update metrics, if something go wrong return null, else - return updated Metric*/
    Metric addOrUpdateMetric(Metric metric);

    /**Remove metric for database*/
    void removeMetric(Metric metric);

    /**Returns all MetricsData for given metrics*/
    List<MetricData> getAllDataForMetrics(Metric metric);

    /**Returns MetricsData for given metrics between startDate and endDate*/
    List<MetricData> getDataForMetrics(Metric metric, Date startDate, Date endDate) throws SQLException;

    /**Add single MetricsData*/
    MetricData addMetricData(Metric metric, MetricData data);

    /**Update single MetricsData*/
    void updateMetricData(MetricData data);

    /**Remove single MetricsData*/
    void removeMetricData(MetricData data);

    /**Remove all MetricsData for given metrics from database*/
    void removeAllMetricsData(Metric metrics) throws SQLException;
}
