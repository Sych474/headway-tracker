package com.gss.database.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

public class TimeMetric implements IMetric {

    public final static String NAME_FIELD_NAME = "name";
    public final static String QUESTION_FIELD_NAME = "question";
    public final static String IS_INTERVAL_FIELD_NAME = "is_interval";
    public final static String IS_ACTIVE_FIELD_NAME = "is_active";
    public final static String ASPECT_FIELD_NAME = "aspect_id";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(unique = true, index = true, dataType = DataType.STRING, columnName = NAME_FIELD_NAME)
    private String name;

    @DatabaseField(dataType = DataType.STRING, columnName = QUESTION_FIELD_NAME)
    private String question;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = IS_INTERVAL_FIELD_NAME)
    private boolean isInterval;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Aspect aspect;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = IS_ACTIVE_FIELD_NAME)
    private boolean isActive;

    public TimeMetric() {
    }

    public TimeMetric(String name, String question, boolean isInterval, Aspect aspect, boolean isActive) {
        this.name = name;
        this.question = question;
        this.isInterval = isInterval;
        this.aspect = aspect;
        this.isActive = isActive;
    }

    public int getId() {
        return id;
    }

    public MetricType getType() { return MetricType.TIME_METRIC; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public boolean isInterval() {
        return isInterval;
    }

    public void setInterval(boolean interval) {
        isInterval = interval;
    }

    public Aspect getAspect() {
        return aspect;
    }

    public void setAspect(Aspect aspect) {
        this.aspect = aspect;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
