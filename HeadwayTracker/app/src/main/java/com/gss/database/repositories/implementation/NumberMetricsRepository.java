package com.gss.database.repositories.implementation;

import com.gss.database.HelperFactory;
import com.gss.database.model.Aspect;
import com.gss.database.model.NumberMetric;
import com.gss.database.model.NumberMetricData;
import com.gss.database.repositories.INumberMetricsRepository;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class NumberMetricsRepository implements INumberMetricsRepository {

    private RuntimeExceptionDao<NumberMetric, Integer> numberMetricsRuntimeDao;
    private RuntimeExceptionDao<NumberMetricData, Integer> numberMetricsDataRuntimeDao;

    public NumberMetricsRepository() {
        this.numberMetricsRuntimeDao = HelperFactory.getHelper().getNumberMetricsRuntimeExceptionDao();
        this.numberMetricsDataRuntimeDao = HelperFactory.getHelper().getNumberMetricsDataRuntimeExceptionDao();
    }

    @Override
    public List<NumberMetric> getRatingMetrics(Aspect aspect, boolean onlyActive) throws SQLException {
        Where<NumberMetric, Integer> query = numberMetricsRuntimeDao.queryBuilder().where().eq(NumberMetric.IS_RATING_FIELD_NAME, true);

        if (aspect != null)
            query = query.and().eq(NumberMetric.ASPECT_FIELD_NAME, aspect);
        if (onlyActive)
            query = query.and().eq(NumberMetric.IS_ACTIVE_FIELD_NAME, true);

        return query.query();
    }

    @Override
    public List<NumberMetric> getQuantitativeMetrics(Aspect aspect, boolean onlyActive) throws SQLException {
        Where<NumberMetric, Integer> query = numberMetricsRuntimeDao.queryBuilder().where().eq(NumberMetric.IS_RATING_FIELD_NAME, false);

        if (aspect != null)
            query = query.and().eq(NumberMetric.ASPECT_FIELD_NAME, aspect);
        if (onlyActive)
            query = query.and().eq(NumberMetric.IS_ACTIVE_FIELD_NAME, true);

        return query.query();
    }

    @Override
    public NumberMetric findMetricsByName(String name) {
        List<NumberMetric> results =  numberMetricsRuntimeDao.queryForEq(NumberMetric.NAME_FIELD_NAME, name);
        if (results.size() == 1)
            return results.get(0);
        else
            return null;
    }

    @Override
    public List<NumberMetric> getMetrics(boolean onlyActive) {
        if (!onlyActive)
            return numberMetricsRuntimeDao.queryForAll();
        else
            return numberMetricsRuntimeDao.queryForEq(NumberMetric.IS_ACTIVE_FIELD_NAME, true);
    }

    @Override
    public List<NumberMetric> getMetricsForAspect(Aspect aspect, boolean onlyActive) throws SQLException {
        Where<NumberMetric, Integer> query = numberMetricsRuntimeDao.queryBuilder().where().eq(NumberMetric.ASPECT_FIELD_NAME, aspect);
        if (onlyActive)
            query =query.and().eq(NumberMetric.IS_ACTIVE_FIELD_NAME, true);

        return query.query();
    }

    @Override
    public NumberMetric addOrUpdateMetric(NumberMetric numberMetric) {
        Dao.CreateOrUpdateStatus status = numberMetricsRuntimeDao.createOrUpdate(numberMetric);
        if (!status.isCreated() && !status.isUpdated())
            return null;
        numberMetricsRuntimeDao.refresh(numberMetric);
        return numberMetric;
    }

    @Override
    public void removeMetric(NumberMetric numberMetric) {
        numberMetricsRuntimeDao.delete(numberMetric);
    }

    @Override
    public List<NumberMetricData> getAllDataForMetrics(NumberMetric numberMetric) {
        return numberMetricsDataRuntimeDao.queryForEq(NumberMetricData.METRIC_FIELD_NAME, numberMetric);
    }

    @Override
    public List<NumberMetricData> getDataForMetrics(NumberMetric numberMetric, Date startDate, Date endDate) throws SQLException {
        return numberMetricsDataRuntimeDao.queryBuilder().where()
                .eq(NumberMetricData.METRIC_FIELD_NAME, numberMetric).and()
                .between(NumberMetricData.DATE_FIELD_NAME, startDate, endDate).query();
    }

    @Override
    public NumberMetricData addMetricData(NumberMetric numberMetric, NumberMetricData numberMetricData) {
        if (numberMetric == null)
            return null;

        numberMetricData.setMetric(numberMetric);
        int result = numberMetricsDataRuntimeDao.create(numberMetricData);
        if (result != 1)
            return null;
        
        numberMetricsDataRuntimeDao.refresh(numberMetricData);
        return numberMetricData;
    }

    @Override
    public void updateMetricData(NumberMetricData numberMetricData) {
        numberMetricsDataRuntimeDao.update(numberMetricData);
    }

    @Override
    public void removeMetricData(NumberMetricData numberMetricData) {
        numberMetricsDataRuntimeDao.delete(numberMetricData);
    }

    @Override
    public void removeAllMetricsData(NumberMetric metric) throws SQLException {
        DeleteBuilder<NumberMetricData, Integer> deleteBuilder  = numberMetricsDataRuntimeDao.deleteBuilder();
        deleteBuilder.where().eq(NumberMetricData.METRIC_FIELD_NAME, metric);
        deleteBuilder.delete();
    }
}
