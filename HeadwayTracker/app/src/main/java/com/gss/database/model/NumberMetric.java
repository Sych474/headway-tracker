package com.gss.database.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

public class NumberMetric implements IMetric {

    public final static String NAME_FIELD_NAME = "name";
    public final static String QUESTION_FIELD_NAME = "question";
    public final static String MAX_VALUE_FIELD_NAME = "max_value";
    public final static String IS_ACTIVE_FIELD_NAME = "is_active";
    public final static String IS_RATING_FIELD_NAME = "is_rating";
    public final static String ASPECT_FIELD_NAME = "aspect_id";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(unique = true, index = true, dataType = DataType.STRING, columnName = NAME_FIELD_NAME)
    private String name;

    @DatabaseField(dataType = DataType.STRING, columnName = QUESTION_FIELD_NAME)
    private String question;

    @DatabaseField(dataType = DataType.INTEGER, columnName = MAX_VALUE_FIELD_NAME)
    private int maxValue;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Aspect aspect;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = IS_ACTIVE_FIELD_NAME)
    private boolean isActive;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = IS_RATING_FIELD_NAME)
    private boolean isRating;

    public NumberMetric() {
    }

    public NumberMetric(String name, String question, Integer maxValue, Aspect aspect, boolean isActive, boolean isRating) {
        this.name = name;
        this.question = question;
        this.maxValue = maxValue;
        this.aspect = aspect;
        this.isActive = isActive;
        this.isRating = isRating;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public MetricType getType() {
        if (isRating)
            return MetricType.RATING_METRIC;
        else
            return MetricType.QUANTITATIVE_METRIC;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Integer getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Integer maxValue) {
        this.maxValue = maxValue;
    }

    public Aspect getAspect() {
        return aspect;
    }

    public void setAspect(Aspect aspect) {
        this.aspect = aspect;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isRating() {
        return isRating;
    }

    public void setRating(boolean rating) {
        isRating = rating;
    }

}
