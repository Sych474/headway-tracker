package com.gss.database.repositories;

import com.gss.database.model.TimeMetric;
import com.gss.database.model.TimeMetricData;

import java.sql.SQLException;
import java.util.List;

/** Describes the methods on table of time metrics*/
public interface ITimeMetricsRepository extends IBaseMetricsRepository<TimeMetric, TimeMetricData> {

    /** Returns all simple (not interval) metrics from database*/
    List<TimeMetric> getTimeSimpleMetrics(boolean onlyActive) throws SQLException;

    /** *Return all interval time metrics from database*/
    List<TimeMetric> getTimeIntervalMetrics(boolean onlyActive) throws SQLException;
}
