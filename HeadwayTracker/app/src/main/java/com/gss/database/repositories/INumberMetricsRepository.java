package com.gss.database.repositories;

import com.gss.database.model.Aspect;
import com.gss.database.model.NumberMetric;
import com.gss.database.model.NumberMetricData;

import java.sql.SQLException;
import java.util.List;

/** Describes the methods on table of numerical metrics*/
public interface INumberMetricsRepository extends IBaseMetricsRepository<NumberMetric, NumberMetricData>{

    /** Returns all Rating metrics for aspect; if aspect is null return all Rating metrics*/
    List<NumberMetric> getRatingMetrics(Aspect aspect, boolean onlyActive) throws SQLException;

    /** *Return all Quantitative metrics from database*/
    List<NumberMetric> getQuantitativeMetrics(Aspect aspect, boolean onlyActive) throws SQLException;
}
