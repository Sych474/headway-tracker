package com.gss.database.model;

public interface IMetric {
    String getName();
    boolean isActive();
    MetricType getType();
}
