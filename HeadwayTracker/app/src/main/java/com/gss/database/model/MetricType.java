package com.gss.database.model;

public enum MetricType {
    QUANTITATIVE_METRIC,
    TIME_METRIC,
    RATING_METRIC,
    TEXT_METRIC
}
