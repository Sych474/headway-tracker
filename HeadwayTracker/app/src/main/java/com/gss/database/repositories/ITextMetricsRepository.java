package com.gss.database.repositories;

import com.gss.database.model.TextMetric;
import com.gss.database.model.TextMetricData;

/** Describes the methods on table of text metrics*/
public interface ITextMetricsRepository extends IBaseMetricsRepository<TextMetric, TextMetricData> {
    // now it is equal to base interface, but in future can be changed.
}
