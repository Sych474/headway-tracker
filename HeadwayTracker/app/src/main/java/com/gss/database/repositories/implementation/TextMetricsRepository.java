package com.gss.database.repositories.implementation;

import com.gss.database.HelperFactory;
import com.gss.database.model.Aspect;
import com.gss.database.model.TextMetric;
import com.gss.database.model.TextMetricData;
import com.gss.database.repositories.ITextMetricsRepository;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class TextMetricsRepository implements ITextMetricsRepository {

    private RuntimeExceptionDao<TextMetric, Integer> textMetricsRuntimeDao;
    private RuntimeExceptionDao<TextMetricData, Integer> textMetricsDataRuntimeDao;

    public TextMetricsRepository() {
        this.textMetricsRuntimeDao = HelperFactory.getHelper().getTextMetricsRuntimeExceptionDao();
        this.textMetricsDataRuntimeDao = HelperFactory.getHelper().getTextMetricsDataRuntimeExceptionDao();
    }

    @Override
    public TextMetric findMetricsByName(String name) {
        List<TextMetric> results =  textMetricsRuntimeDao.queryForEq(TextMetric.NAME_FIELD_NAME, name);
        if (results.size() == 1)
            return results.get(0);
        else
            return null;
    }

    @Override
    public List<TextMetric> getMetrics(boolean onlyActive) {
        if (!onlyActive)
            return textMetricsRuntimeDao.queryForAll();
        else
            return textMetricsRuntimeDao.queryForEq(TextMetric.IS_ACTIVE_FIELD_NAME, true);
    }

    @Override
    public List<TextMetric> getMetricsForAspect(Aspect aspect, boolean onlyActive) throws SQLException {
        Where<TextMetric, Integer> query = textMetricsRuntimeDao.queryBuilder().where().eq(TextMetric.ASPECT_FIELD_NAME, aspect);
        if (onlyActive)
            query =query.and().eq(TextMetric.IS_ACTIVE_FIELD_NAME, true);

        return query.query();
    }

    @Override
    public TextMetric addOrUpdateMetric(TextMetric textMetric) {

        Dao.CreateOrUpdateStatus status = textMetricsRuntimeDao.createOrUpdate(textMetric);
        if (!status.isCreated() && !status.isUpdated())
            return null;
        textMetricsRuntimeDao.refresh(textMetric);
        return textMetric;
    }

    @Override
    public void removeMetric(TextMetric textMetric) {
        textMetricsRuntimeDao.delete(textMetric);
    }

    @Override
    public List<TextMetricData> getAllDataForMetrics(TextMetric textMetric) {
        return textMetricsDataRuntimeDao.queryForEq(TextMetricData.METRIC_FIELD_NAME, textMetric);
    }

    @Override
    public List<TextMetricData> getDataForMetrics(TextMetric textMetric, Date startDate, Date endDate) throws SQLException {
        return textMetricsDataRuntimeDao.queryBuilder().where()
            .eq(TextMetricData.METRIC_FIELD_NAME, textMetric).and()
            .between(TextMetricData.DATE_FIELD_NAME, startDate, endDate).query();
    }

    @Override
    public TextMetricData addMetricData(TextMetric metric, TextMetricData textMetricData) {
        if (metric == null)
            return null;

        textMetricData.setMetric(metric);
        int result = textMetricsDataRuntimeDao.create(textMetricData);
        if (result != 1)
            return null;

        textMetricsDataRuntimeDao.refresh(textMetricData);
        return textMetricData;
    }

    @Override
    public void updateMetricData(TextMetricData textMetricData) {
        textMetricsDataRuntimeDao.update(textMetricData);
    }

    @Override
    public void removeMetricData(TextMetricData textMetricData) {
        textMetricsDataRuntimeDao.delete(textMetricData);
    }

    @Override
    public void removeAllMetricsData(TextMetric textMetric) throws SQLException {
        DeleteBuilder<TextMetricData, Integer> deleteBuilder  = textMetricsDataRuntimeDao.deleteBuilder();
        deleteBuilder.where().eq(TextMetricData.METRIC_FIELD_NAME, textMetric);
        deleteBuilder.delete();
    }
}
