package com.gss.headwaytracker;

import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gss.database.model.TextMetric;
import com.gss.database.model.TextMetricData;
import com.gss.database.repositories.implementation.TextMetricsRepository;
import com.gss.headwaytracker.survey.surveyManager.SurveyManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TextMetricActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnPrev;
    private int metricsCounter;

    private TextMetricsRepository textMetricsRepository = new TextMetricsRepository();
    private List<TextMetric> textMetrics;
    private List<EditText> answers;

    private boolean isAlreadyTracked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_metric);

        metricsCounter = 0;
        try {
            textMetrics = textMetricsRepository.getMetricsForAspect(SurveyManager.getCurrentAspect(), true);
        } catch (SQLException e) {
            //TODO show message
            e.printStackTrace();
        }

        answers = new ArrayList<>();
        generateTextMetricView(textMetrics.get(metricsCounter));

        Button btnNext = findViewById(R.id.MetricsNextButton);
        btnPrev = findViewById(R.id.MetricsPrevButton);

        btnNext.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.MetricsNextButton:
                moveNext();
                break;
            case R.id.MetricsPrevButton:
                movePrev();
                break;
            default:
                break;
        }
    }

    /** Generates activity view with Aspect name, Metric name, Question and few lines for input */
    private void generateTextMetricView(TextMetric metric) {
        btnPrev = findViewById(R.id.MetricsPrevButton);
        if (SurveyManager.getCurrentIndex() == 0)
            btnPrev.setVisibility(View.INVISIBLE);
        else
            btnPrev.setVisibility(View.VISIBLE);

        TextView headerAspect = findViewById(R.id.headerAspectMetricsText);
        headerAspect.setText(metric.getAspect().getName());

        TextView nameMetrics = findViewById(R.id.nameMetricsText);
        nameMetrics.setText(metric.getName());

        TextView questionMetrics = findViewById(R.id.questionMetricsText);
        questionMetrics.setText(metric.getQuestion());

        LinearLayout layout = findViewById(R.id.containerAnswersMetricsText);
        layout.removeAllViews();
        for (int i = 0; i < metric.getAnswerCount(); i++) {
            EditText answer = new EditText(getApplicationContext());
            answer.setBackgroundTintList(ColorStateList.valueOf(0xFF008577));
            answer.setTextColor(Color.DKGRAY);
            answers.add(answer);
            layout.addView(answer);
        }

        isAlreadyTracked = loadData();
    }

    private boolean moveNextView(int index) {
        if (index < textMetrics.size()) {
            generateTextMetricView(textMetrics.get(index));
            return true;
        }
        return false;
    }

    private boolean movePrevView(int index) {
        if (index >= 0) {
            generateTextMetricView(textMetrics.get(index));
            return true;
        }
        return false;
    }

    private void movePrevActivity(TextMetricActivity context) {
        SurveyManager.undoStep();
        SurveyManager.moveToNextActivity(context);
    }

    private void moveNextActivity(TextMetricActivity context) {
        SurveyManager.doStep();
        SurveyManager.moveToNextActivity(context);
    }

    /** Save answers from current activity to database */
    private void saveData() {
        List<String> stringAnswers = Instrumentation.convertTextEditsToStringList(answers);
        TextMetric metric = textMetrics.get(metricsCounter);
        TextMetricData data;

        if (isAlreadyTracked) {
            data = getDataFromDatabase().get(0);
            data.setValue(stringAnswers);
            textMetricsRepository.updateMetricData(data);
        }
        else {
            Date now = new Date();
            Date date = new Date(now.getYear(), now.getMonth(), now.getDate(),0, 0, 0);
            data = new TextMetricData("", date, metric);
            data.setValue(stringAnswers);
            textMetricsRepository.addMetricData(metric, data);
        }
    }

    private void sendErrorMessage() {
        AlertDialog sendErrorDialog = DialogScreen.getDialog(
                this, DialogScreen.IDD_SEND_ERROR_MESSAGE);
        if (sendErrorDialog != null) {
            sendErrorDialog.show();
        }
    }

    /** Load answers to current activity from database */
    private boolean loadData() {
        List<TextMetricData> dataList = getDataFromDatabase();
        if (dataList.isEmpty())
            return false;

        TextMetricData data = dataList.get(0);
        List<String> answersList = data.getValue();
        for (int i = 0; i < answersList.size(); i++) {
            EditText answerEdit = answers.get(i);
            answerEdit.setText(answersList.get(i));
            answerEdit.setTextColor(Color.DKGRAY);
            answerEdit.setBackgroundTintList(ColorStateList.valueOf(0xFF008577));
        }

        return true;
    }

    private List<TextMetricData> getDataFromDatabase() {
        Date now = new Date();
        Date begin = new Date(now.getYear(), now.getMonth(), now.getDate(),0, 0, 0);
        Date end = new Date(now.getYear(), now.getMonth(), now.getDate(),23, 59, 59);

        List<TextMetricData> dataList = new ArrayList<>();
        try {
            dataList = textMetricsRepository.getDataForMetrics(textMetrics.get(metricsCounter), begin, end);
        } catch (SQLException e) {
            sendErrorMessage();
        }
        return dataList;
    }

    private void moveNext() {
        saveData();
        if (!moveNextView(++metricsCounter))
            moveNextActivity(this);
    }

    private void movePrev() {
        if (!movePrevView(--metricsCounter))
            movePrevActivity(this);
    }

    @Override
    public void onBackPressed() {
        movePrev();
    }
}
