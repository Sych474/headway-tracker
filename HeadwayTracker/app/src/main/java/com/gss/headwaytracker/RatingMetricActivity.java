package com.gss.headwaytracker;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.gss.database.model.NumberMetric;
import com.gss.database.model.NumberMetricData;
import com.gss.database.repositories.implementation.NumberMetricsRepository;
import com.gss.headwaytracker.survey.surveyManager.SurveyManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RatingMetricActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnPrev;
    private RatingBar input;

    private int metricsCounter;
    private NumberMetricsRepository numberMetricsRepository = new NumberMetricsRepository();
    private List<NumberMetric> numberMetrics;

    private boolean isAlreadyTracked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_metric);

        metricsCounter = 0;
        try {
            numberMetrics = numberMetricsRepository.getRatingMetrics(SurveyManager.getCurrentAspect(), true);
        } catch (SQLException e) {
            sendErrorMessage();
            e.printStackTrace();
        }
        generateNumberMetricView(numberMetrics.get(metricsCounter));

        input = findViewById(R.id.ratingBar);

        Button btnNext = findViewById(R.id.MetricsNextButton);
        btnPrev = findViewById(R.id.MetricsPrevButton);

        btnNext.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.MetricsNextButton:
                moveNext();
                break;
            case R.id.MetricsPrevButton:
                movePrev();
                break;
            default:
                break;
        }
    }

    /** Generates number metric View with name of aspect, name of metric, question and input field */
    private void generateNumberMetricView (NumberMetric metric) {
        btnPrev = findViewById(R.id.MetricsPrevButton);
        if (SurveyManager.getCurrentIndex() == 0)
            btnPrev.setVisibility(View.INVISIBLE);
        else
            btnPrev.setVisibility(View.VISIBLE);

        TextView headerAspect = findViewById(R.id.headerAspectMetricNumber);
        headerAspect.setText(metric.getAspect().getName());

        TextView name = findViewById(R.id.nameMetricNumber);
        name.setText(metric.getName());

        TextView question = findViewById(R.id.questionMetricNumber);
        question.setText(metric.getQuestion());

        isAlreadyTracked = loadData();
    }

    /** Save answers from current activity to database */
    private void saveData(int value) {
        NumberMetric metric = numberMetrics.get(metricsCounter);
        NumberMetricData data;

        if (isAlreadyTracked) {
            data = getDataFromDatabase().get(0);
            data.setValue(value);
            numberMetricsRepository.updateMetricData(data);
        }
        else {
            Date now = new Date();
            Date date = new Date(now.getYear(), now.getMonth(), now.getDate(),0, 0, 0);
            data = new NumberMetricData(value, date, metric);
            numberMetricsRepository.addMetricData(metric, data);
        }
    }

    /** Load answers to current activity from database */
    private boolean loadData() {
        input = findViewById(R.id.ratingBar);

        List<NumberMetricData> dataList = getDataFromDatabase();
        if (dataList.isEmpty())
            return false;

        NumberMetricData data = dataList.get(0);
        input.setRating(data.getValue());

        return true;
    }

    private List<NumberMetricData> getDataFromDatabase() {
        Date now = new Date();
        Date begin = new Date(now.getYear(), now.getMonth(), now.getDate(),0, 0, 0);
        Date end = new Date(now.getYear(), now.getMonth(), now.getDate(),23, 59, 59);

        List<NumberMetricData> dataList = new ArrayList<>();
        try {
            dataList = numberMetricsRepository.getDataForMetrics(numberMetrics.get(metricsCounter), begin, end);
        } catch (SQLException e) {
            sendErrorMessage();
        }
        return dataList;
    }

    private boolean moveNextView(int index) {
        if (index < numberMetrics.size()) {
            generateNumberMetricView(numberMetrics.get(index));
            return true;
        }
        return false;
    }

    private boolean movePrevView(int index) {
        if (index >= 0) {
            generateNumberMetricView(numberMetrics.get(index));
            return true;
        }
        return false;
    }

    private void movePrevActivity(RatingMetricActivity context) {
        SurveyManager.undoStep();
        SurveyManager.moveToNextActivity(context);
    }

    private void moveNextActivity(RatingMetricActivity context) {
        SurveyManager.doStep();
        SurveyManager.moveToNextActivity(context);
    }

    private void sendErrorMessage() {
        AlertDialog sendErrorDialog = DialogScreen.getDialog(
                this, DialogScreen.IDD_SEND_ERROR_MESSAGE);
        if (sendErrorDialog != null) {
            sendErrorDialog.show();
        }
    }

    private void moveNext(){
        int value = (int) input.getRating();
        saveData(value);

        if (!moveNextView(++metricsCounter))
            moveNextActivity(this);
    }

    private void movePrev() {
        if (!movePrevView(--metricsCounter))
            movePrevActivity(this);
    }

    @Override
    public void onBackPressed() {
        movePrev();
    }
}
