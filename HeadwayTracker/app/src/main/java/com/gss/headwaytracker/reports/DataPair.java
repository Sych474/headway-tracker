package com.gss.headwaytracker.reports;

public class DataPair<T> {
    private String date;
    private T value;

    public DataPair(String date, T value) {
        this.date = date;
        this.value = value;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
