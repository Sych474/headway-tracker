package com.gss.headwaytracker;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.gss.database.model.Aspect;
import com.gss.database.model.NumberMetric;
import com.gss.database.model.TextMetric;
import com.gss.database.model.TimeMetric;
import com.gss.database.repositories.implementation.NumberMetricsRepository;
import com.gss.database.repositories.implementation.TextMetricsRepository;
import com.gss.database.repositories.implementation.TimeMetricsRepository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

class AspectSettingElement {
    private Aspect aspect;
    private Switch aspectSwitch;

    private List<TextMetric> textMetrics;
    private List<NumberMetric> numberMetrics;
    private List<TimeMetric> timeMetrics;

    private List<Switch> textSwitches;
    private List<Switch> numberSwitches;
    private List<Switch> timeSwitches;

    private TextMetricsRepository textMetricsRepository = new TextMetricsRepository();
    private TimeMetricsRepository timeMetricsRepository = new TimeMetricsRepository();
    private NumberMetricsRepository numberMetricsRepository = new NumberMetricsRepository();

    private Context context;
    private LinearLayout layout;

    AspectSettingElement(Aspect aspect, Context context, LinearLayout layout) {
        this.aspect = aspect;
        this.context = context;
        this.layout = layout;
        boolean isActive = aspect.isActive();

        this.aspectSwitch = new Switch(this.context);
        this.aspectSwitch.setText(aspect.getName());
        this.aspectSwitch.setChecked(isActive);
        this.aspectSwitch.setTextSize(20);
        this.aspectSwitch.setTextColor(Color.DKGRAY);
        this.aspectSwitch.setTextDirection(View.TEXT_DIRECTION_INHERIT);
        layout.addView(this.aspectSwitch);

        try {
            generateTextSwitches();
            generateNumberSwitches();
            generateTimeSwitches();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        aspectSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    setAllSwitchesOff();
                }
                else {
                    setAllSwitchesAsDefault();
                }
            }
        });
    }

    /** Generates switches for text metrics of the aspect */
    private void generateTextSwitches() throws SQLException {
        textMetrics = textMetricsRepository.getMetricsForAspect(aspect, false);
        textSwitches = new ArrayList<>();

        for (TextMetric metric : textMetrics) {
            Switch metricSwitch = new Switch(context);
            metricSwitch.setText(metric.getName());
            metricSwitch.setTextColor(Color.BLACK);

            metricSwitch.setChecked(metric.isActive());

            textSwitches.add(metricSwitch);
            layout.addView(metricSwitch);
        }
    }

    /** Generates switches for time metrics of the aspect */
    private void generateTimeSwitches() throws SQLException {
        timeMetrics = timeMetricsRepository.getMetricsForAspect(aspect, false);
        timeSwitches = new ArrayList<>();

        for (TimeMetric metric : timeMetrics) {
            Switch metricSwitch = new Switch(context);
            metricSwitch.setText(metric.getName());
            metricSwitch.setTextColor(Color.BLACK);
            metricSwitch.setChecked(metric.isActive());

            timeSwitches.add(metricSwitch);
            layout.addView(metricSwitch);
        }
    }

    /** Generates switches for number metrics of the aspect */
    private void generateNumberSwitches() throws SQLException {
        numberMetrics = numberMetricsRepository.getMetricsForAspect(aspect, false);
        numberSwitches = new ArrayList<>();

        for (NumberMetric metric : numberMetrics) {
            Switch metricSwitch = new Switch(context);
            metricSwitch.setText(metric.getName());
            metricSwitch.setTextColor(Color.BLACK);
            metricSwitch.setChecked(metric.isActive());

            numberSwitches.add(metricSwitch);
            layout.addView(metricSwitch);
        }
    }

    /** Updates values of metric fields "isActive" according to metric switches */
    void updateMetricsDataBase() {
        for (int i = 0; i < textSwitches.size(); i++) {
            TextMetric metric = textMetrics.get(i);
            Switch switchText = textSwitches.get(i);

            metric.setActive(switchText.isChecked());
            textMetricsRepository.addOrUpdateMetric(metric);
        }

        for (int i = 0; i < numberSwitches.size(); i++) {
            NumberMetric metric = numberMetrics.get(i);
            Switch switchNumber = numberSwitches.get(i);

            metric.setActive(switchNumber.isChecked());
            numberMetricsRepository.addOrUpdateMetric(metric);
        }

        for (int i = 0; i < timeSwitches.size(); i++) {
            TimeMetric metric = timeMetrics.get(i);
            Switch switchTime = timeSwitches.get(i);

            metric.setActive(switchTime.isChecked());
            timeMetricsRepository.addOrUpdateMetric(metric);
        }
    }

    /** Returns true if aspect switch is on, returns false - if aspect switch is off */
    boolean isAspectChecked() {
        return aspectSwitch.isChecked();
    }

    private void setAllSwitchesOff() {
        for (Switch s : textSwitches)
            s.setChecked(false);
        for (Switch s : timeSwitches)
            s.setChecked(false);
        for (Switch s : numberSwitches)
            s.setChecked(false);
    }

    private void setAllSwitchesAsDefault() {
        for (int i = 0; i < textSwitches.size(); i++)
            textSwitches.get(i).setChecked( textMetrics.get(i).isActive());
        for (int i = 0; i < timeSwitches.size(); i++)
            timeSwitches.get(i).setChecked( timeMetrics.get(i).isActive());
        for (int i = 0; i < numberSwitches.size(); i++)
            numberSwitches.get(i).setChecked( numberMetrics.get(i).isActive());
    }
}
