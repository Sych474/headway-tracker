package com.gss.headwaytracker.survey.surveyManager;

import android.content.Context;
import android.content.Intent;

import com.gss.database.model.Aspect;
import com.gss.database.repositories.INumberMetricsRepository;
import com.gss.database.repositories.ITextMetricsRepository;
import com.gss.database.repositories.ITimeMetricsRepository;
import com.gss.headwaytracker.DailyReportActivity;
import com.gss.headwaytracker.MainActivity;
import com.gss.headwaytracker.RatingMetricActivity;
import com.gss.headwaytracker.TextMetricActivity;
import com.gss.headwaytracker.TimeMetricActivity;
import com.gss.headwaytracker.NumberMetricActivity;
import com.gss.headwaytracker.survey.SurveyStatusType;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/** Class to coordinate user polling process. */
public class SurveyManager {

    private static List<SurveyElement> surveyElements;
    private static List<Aspect> activeAspects;
    private static int currentIndex;

    private static ITextMetricsRepository textMetricsRepository;
    private static INumberMetricsRepository numberMetricsRepository;
    private static ITimeMetricsRepository timeMetricsRepository;

    /** Set repositories, this method should be called when application starts */
    public static void setRepositories(ITextMetricsRepository textMetricsRepo, INumberMetricsRepository numberMetricsRepo, ITimeMetricsRepository timeMetricsRepo) {
        textMetricsRepository = textMetricsRepo;
        numberMetricsRepository = numberMetricsRepo;
        timeMetricsRepository = timeMetricsRepo;
    }

    /** Starts new polling session using given aspects return false if can`t start new survey */
    public static boolean init(List<Aspect> aspects) {
        surveyElements = generateSurveyElements(aspects);
        if (surveyElements.size() == 0)
            return false;

        activeAspects = aspects;

        currentIndex = 0;
        return true;
    }

    /** Returns the list of given aspects */
    public static List<Aspect> GetAspects() {
        return activeAspects;
    }

    /** Returns the type of the Activity that should be showed on current step */
    public static SurveyStatusType getCurrentStatusType() {
        return surveyElements.get(currentIndex).getElementStatusType();
    }

    /** Returns the aspect for which the Activity should be showed on current step */
    public static Aspect getCurrentAspect() {
        return surveyElements.get(currentIndex).getElementAspect();
    }

    /** Command for manager to go to the next step, returns next activity type or null if can`t do next step (survey ended) */
    public static SurveyStatusType doStep() {
        if (currentIndex == surveyElements.size()-1)
            return null;

        currentIndex += 1;
        return getCurrentStatusType();
    }

    /** Command for manager to go to the previous step, returns previous activity type or null if can`t go back (current step is first) */
    public static SurveyStatusType undoStep() {
        if (currentIndex == 0)
            return null;

        currentIndex -= 1;
        return getCurrentStatusType();
    }

    /** Changes activity */
    public static void moveToNextActivity(Context context) {
        Intent intent;

        switch(SurveyManager.getCurrentStatusType()) {
            case TEXT_METRICS:
                intent = new Intent(context, TextMetricActivity.class);
                context.startActivity(intent);
                break;
            case TIME_METRICS:
                intent = new Intent(context, TimeMetricActivity.class);
                context.startActivity(intent);
                break;
            case QUANTITATIVE_METRICS:
                intent = new Intent(context, NumberMetricActivity.class);
                context.startActivity(intent);
                break;
            case RATING_METRICS:
                intent = new Intent(context, RatingMetricActivity.class);
                context.startActivity(intent);
                break;
            case REPORT:
                intent = new Intent(context, DailyReportActivity.class);
                context.startActivity(intent);
                break;
            default:
                intent = new Intent(context, MainActivity.class);
                context.startActivity(intent);
                break;
        }
    }

    /** Generates list of SurveyElements for all given aspects, if aspect have no metrics of some type SurveyElement will not be created.
     * If there are more then zero SurveyElement for metrics, REPORT SurveyElement will be added to the end of the generated list */
    private static List<SurveyElement> generateSurveyElements(List<Aspect> aspects) {
        List<SurveyElement> elements = new ArrayList<>();

        for (Aspect aspect: aspects) {
            try {
                if (textMetricsRepository.getMetricsForAspect(aspect, false).size() > 0)
                    elements.add(new SurveyElement(SurveyStatusType.TEXT_METRICS, aspect));

                if (timeMetricsRepository.getMetricsForAspect(aspect, false).size() > 0)
                    elements.add(new SurveyElement(SurveyStatusType.TIME_METRICS, aspect));

                if (numberMetricsRepository.getQuantitativeMetrics(aspect, false).size() > 0)
                    elements.add(new SurveyElement(SurveyStatusType.QUANTITATIVE_METRICS, aspect));

                if (numberMetricsRepository.getRatingMetrics(aspect, false).size() > 0)
                    elements.add(new SurveyElement(SurveyStatusType.RATING_METRICS, aspect));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (elements.size() != 0)
            elements.add(new SurveyElement(SurveyStatusType.REPORT, null));

        return elements;
    }

    public static int getCurrentIndex() {
        return currentIndex;
    }
}
