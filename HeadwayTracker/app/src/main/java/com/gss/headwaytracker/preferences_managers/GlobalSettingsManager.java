package com.gss.headwaytracker.preferences_managers;

import android.content.Context;

import com.gss.headwaytracker.R;

public class GlobalSettingsManager {
    private SharedPreferencesManager manager;
    private String guestField;
    private String firstInitField;

    public GlobalSettingsManager(Context context) {
        String name = context.getString(R.string.global_settings_name);
        manager = new SharedPreferencesManager(context, name);

        guestField = context.getString(R.string.gs_guest_field_name);
        firstInitField = context.getString(R.string.gs_first_init_field_name);
    }

    public void putGuestName(String value) {
        manager.putString(guestField, value);
    }

    public void putFirstInitState(Boolean value) {
        manager.putBoolean(firstInitField, value);
    }

    public String getGuestName() {
        return manager.getString(guestField);
    }

    public Boolean isFirstInitialized() {
        return manager.getBoolean(firstInitField, false);
    }
}
