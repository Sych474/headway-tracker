package com.gss.headwaytracker;

import android.app.Application;
import com.gss.database.HelperFactory;
import com.gss.database.repositories.implementation.NumberMetricsRepository;
import com.gss.database.repositories.implementation.TextMetricsRepository;
import com.gss.database.repositories.implementation.TimeMetricsRepository;
import com.gss.headwaytracker.survey.surveyManager.SurveyManager;

public class MainApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        HelperFactory.setHelper(getApplicationContext());
        SurveyManager.setRepositories(new TextMetricsRepository(), new NumberMetricsRepository(), new TimeMetricsRepository());
    }

    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }
}