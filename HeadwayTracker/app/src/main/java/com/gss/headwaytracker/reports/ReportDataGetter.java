package com.gss.headwaytracker.reports;

import android.graphics.Color;

import com.gss.database.model.Aspect;
import com.gss.database.model.IMetric;
import com.gss.database.model.IMetricData;
import com.gss.database.model.NumberMetric;
import com.gss.database.model.NumberMetricData;
import com.gss.database.model.TimeMetric;
import com.gss.database.model.TimeMetricData;
import com.gss.database.repositories.IAspectsRepository;
import com.gss.database.repositories.INumberMetricsRepository;
import com.gss.database.repositories.ITimeMetricsRepository;
import com.gss.database.repositories.implementation.AspectsRepository;
import com.gss.database.repositories.implementation.NumberMetricsRepository;
import com.gss.database.repositories.implementation.TimeMetricsRepository;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ReportDataGetter {

    private ITimeMetricsRepository timeMetricsRepository;
    private INumberMetricsRepository numberMetricsRepository;
    private IAspectsRepository aspectRepository;

    private static final long MS_IN_MINUTE = (1000*60);
    private static final long MS_IN_HOUR = (1000*60*60);
    private static final long MS_IN_DAY = (1000*60*60*24);


    public ReportDataGetter(ITimeMetricsRepository timeMetricsRepository, INumberMetricsRepository numberMetricsRepository, IAspectsRepository aspectRepository) {
        this.timeMetricsRepository = timeMetricsRepository;
        this.numberMetricsRepository = numberMetricsRepository;
        this.aspectRepository = aspectRepository;
    }

    public ReportDataGetter() {
        this.timeMetricsRepository = new TimeMetricsRepository();
        this.numberMetricsRepository = new NumberMetricsRepository();
        this.aspectRepository = new AspectsRepository();
    }

    /** Returns an instance of Date class which corresponds to the start of today */
    public static Date getStartOfToday() {
        // Set dates
        Date startOfToday = new Date();

        long offset = startOfToday.getTime() % MS_IN_DAY;

        startOfToday.setTime(startOfToday.getTime() - offset);

        return startOfToday;
    }

    /** Returns an instance of Date class with current time */
    public static Date getCurrentDate() {
        return new Date();
    }

    /** Returns a list of different colors presented by integer with given size */
    public static List<Integer> getColorList(int size) {
        List<Integer> colors = new ArrayList<>();

        int[] rgb = new int[3];
        int switcher = 0;

        for (int i = 1; i < size+1; i++) {
            rgb[switcher] += i * 100;
            rgb[switcher] %= 255;
            switcher = i % 3;

            colors.add(Color.rgb(rgb[0], rgb[1], rgb[2]));
        }

        return colors;
    }

    public List<Aspect> getAllActiveAspects() {
        return aspectRepository.getAspects(true);
    }

    public List<IMetric> getAllActiveMetrics(Aspect aspect) {
        List<IMetric> metrics = new ArrayList<>();
        try {
            metrics.addAll(numberMetricsRepository.getMetricsForAspect(aspect, true));
            metrics.addAll(timeMetricsRepository.getMetricsForAspect(aspect, true));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

        return metrics;
    }

    /** Returns sum of time spent on given activity in minutes for given time interval (start; end) */
    public long getSumTimeForAspect(Aspect aspect, Date start, Date end) {
        List<TimeMetric> metrics = getAllTimeMetricsForAspectForToday(aspect);
        if (metrics == null)
            return 0;

        long res = 0;

        for (int i = 0; i < metrics.size(); i++) {
            List<TimeMetricData> data;
            try {
                data = timeMetricsRepository.getDataForMetrics(
                        metrics.get(i), start, end
                );
            }
            catch (SQLException e) {
                data = new ArrayList<>();
            }

            long sum = 0;
            for (int j = 0; j < data.size(); j++) {
                sum += data.get(j).getStartTime().getTime();
            }

            res += sum;
        }

        return res / MS_IN_MINUTE;
    }

    /** Returns a list of TimeMetrics that have data for today */
    public List<TimeMetric> getAllTimeMetricsForAspectForToday(Aspect aspect) {
        return getAllTimeMetricsForAspect(
                aspect,
                ReportDataGetter.getStartOfToday(),
                ReportDataGetter.getCurrentDate()
        );
    }

    /** Returns a list of TimeMetrics that have data for given time interval (start; end) */
    public List<TimeMetric> getAllTimeMetricsForAspect(Aspect aspect, Date start, Date end) {
        List<TimeMetric> timeMetrics;
        try {
            timeMetrics = timeMetricsRepository.getMetricsForAspect(aspect, false);
        } catch (SQLException e) {
            return null;
        }

        if (timeMetrics.isEmpty())
            return null;

        List<TimeMetric> resMetrics = new ArrayList<>();
        for (int i = 0; i < timeMetrics.size(); i++) {
            if (hasTimeMetricData(timeMetrics.get(i), start, end)){
                resMetrics.add(timeMetrics.get(i));
            }
        }

        return resMetrics;
    }

    /** Returns true when metric has data for given time interval (start; end) */
    public boolean hasTimeMetricData(TimeMetric metric, Date start, Date end) {
        try {
            List<TimeMetricData> timeMetricData = timeMetricsRepository.getDataForMetrics(
                    metric, start, end
            );
            return timeMetricData.size() > 0;
        }
        catch (SQLException e) {
            return false;
        }
    }

    /** Returns a list of rating metrics for given aspects for today */
    public List<NumberMetric> getAllRatingMetricsForToday(List<Aspect> aspects) {
        return getAllRatingMetrics(
                aspects,
                ReportDataGetter.getStartOfToday(),
                ReportDataGetter.getCurrentDate()
        );
    }

    /** Returns a list of rating metrics for given aspects for given time interval (start; end) */
    public List<NumberMetric> getAllRatingMetrics(List<Aspect> aspects, Date start, Date end) {
        List<NumberMetric> res = new ArrayList<>();

        for (int i = 0; i < aspects.size(); i++) {
            List<NumberMetric> currentList = getAllRatingMetricsForAspect(aspects.get(i), start, end);
            if (currentList != null){
                res.addAll(currentList);
            }
        }

        return res;
    }

    /** Returns a list of rating metrics for given aspect for given time interval (start; end) */
    public List<NumberMetric> getAllRatingMetricsForAspect(Aspect aspect, Date start, Date end) {
        List<NumberMetric> numberMetrics;
        try {
            numberMetrics = numberMetricsRepository.getRatingMetrics(aspect, false);
        }
        catch (SQLException e) {
            return null;
        }

        if (numberMetrics.isEmpty())
            return null;

        List<NumberMetric> resMetrics = new ArrayList<>();
        for (int i = 0; i < numberMetrics.size(); i++) {
            if (hasNumberMetricData(numberMetrics.get(i), start, end)){
                resMetrics.add(numberMetrics.get(i));
            }
        }

        return resMetrics;
    }

    /** Returns true when metric has data for given time interval (start; end) */
    public boolean hasNumberMetricData(NumberMetric metric, Date start, Date end) {
        try {
            List<NumberMetricData> numberMetricData = numberMetricsRepository.getDataForMetrics(
                    metric, start, end
            );
            return numberMetricData.size() > 0;
        }
        catch (SQLException e) {
            return false;
        }
    }

    /** Returns the last answer for given number metric */
    public NumberMetricData getLastDataForNumberMetric(NumberMetric metric) {
        List<NumberMetricData> dataList = numberMetricsRepository.getAllDataForMetrics(metric);

        NumberMetricData lastData = dataList.get(0);
        for (int i = 0; i < dataList.size(); i++) {
            NumberMetricData data = dataList.get(i);

            if (data.getDate().getTime() > lastData.getDate().getTime())
                lastData = dataList.get(i);
        }

        return lastData;
    }

    /** Returns a list of quantitative metrics for given aspects for given time interval (start; end) */
    public List<NumberMetric> getAllQuantitativeMetrics(List<Aspect> aspects, Date start, Date end) {
        List<NumberMetric> res = new ArrayList<>();

        for (int i = 0; i < aspects.size(); i++) {
            List<NumberMetric> currentList = getAllQuantitativeMetricsForAspect(aspects.get(i), start, end);
            if (currentList != null){
                res.addAll(currentList);
            }
        }

        return res;
    }

    /** Returns a list of quantitative metrics for given aspect for given time interval (start; end) */
    public List<NumberMetric> getAllQuantitativeMetricsForAspect(Aspect aspect, Date start, Date end) {
        List<NumberMetric> numberMetrics;
        try {
            numberMetrics = numberMetricsRepository.getQuantitativeMetrics(aspect, false);
        }
        catch (SQLException e) {
            return null;
        }

        if (numberMetrics.isEmpty())
            return null;

        List<NumberMetric> resMetrics = new ArrayList<>();
        for (int i = 0; i < numberMetrics.size(); i++) {
            if (hasNumberMetricData(numberMetrics.get(i), start, end)){
                resMetrics.add(numberMetrics.get(i));
            }
        }

        return resMetrics;
    }

    public  List<DataPair<Integer>> getDataForRatingMetric(NumberMetric metric, Date startDate, Date endDate) throws Exception {
        if (!metric.isRating())
            throw new Exception("metric is not rating");

        return getDataForNumberMetric(metric, startDate, endDate);
    }

    public  List<DataPair<Integer>> getDataForQuantitativeMetric(NumberMetric metric, Date startDate, Date endDate) throws Exception{
        if (metric.isRating())
            throw new Exception("metric is not rating");

        return getDataForNumberMetric(metric, startDate, endDate);
    }

    public  List<DataPair<Integer>> getDataForTimeMetric(TimeMetric metric, Date startDate, Date endDate) throws SQLException {

        List<TimeMetricData> dataList = timeMetricsRepository.getDataForMetrics(metric, startDate, endDate);

        List<DataPair<Integer>> result = new ArrayList<>();

        Date start = getNormalizedDate(startDate);
        Date end = getNormalizedDate(endDate);

        long daysCount = getDaysDiff(start, end);

        for (int i = 0; i < daysCount; i++) {
            Date currentDate = new Date(start.getTime() + i * MS_IN_DAY);
            TimeMetricData currentData = (TimeMetricData) findMetricDataByDate( new ArrayList<IMetricData>(dataList), currentDate);
            if (currentData == null){
                result.add(new DataPair<>(getFormattedDate(currentDate), 0));
            }
            else {
                result.add(new DataPair<>(getFormattedDate(currentDate), (int) (long)(currentData.getIntervalInMiliseconds() / MS_IN_MINUTE)));
            }
        }

        return result;
    }

    public  List<DataPair<Integer>> getAverageDataForRatingMetric(NumberMetric metric, Date startDate, Date endDate) throws Exception {
        if (!metric.isRating())
            throw new Exception("metric is not rating");

        List<NumberMetricData> dataList = numberMetricsRepository.getDataForMetrics(metric, startDate, endDate);

        List<DataPair<Integer>> result = new ArrayList<>();

        int[] count = new int[metric.getMaxValue()];

        for (NumberMetricData data : dataList) {
            count[data.getValue()-1]++;
        }

        for (int i = 0; i < metric.getMaxValue(); i++){
            if (count[i] != 0)
                result.add(new DataPair<>(Integer.toString(i + 1), count[i]));
        }

        return result;
    }

    public  List<DataPair<Integer>> getAverageTimesForAspects(List<Aspect> aspects, Date startDate, Date endDate) throws SQLException {
        List<DataPair<Integer>> result = new ArrayList<>();
        long[] times = new long[aspects.size()];

        Date start = getNormalizedDate(startDate);
        Date end = getNormalizedDate(endDate);

        long daysCount = getDaysDiff(start, end);

        for (int i = 0; i < aspects.size(); i++){
            List<TimeMetric> metrics = timeMetricsRepository.getMetricsForAspect(aspects.get(i), true);
            times[i] = 0;
            for (TimeMetric metric : metrics) {
                for (TimeMetricData data : timeMetricsRepository.getDataForMetrics(metric, startDate, endDate)) {
                    times[i] += data.getIntervalInMiliseconds();
                }
            }
            times[i] = times[i] / MS_IN_MINUTE / daysCount;
        }

        for (int i = 0; i < aspects.size(); i++){
            if (times[i] != 0)
                result.add(new DataPair<>(aspects.get(i).getName(), (int) (long) times[i]));
        }


        return  result;
    }

    private long getDaysDiff(Date start, Date end){
        double days = ((end.getTime() - (double) start.getTime()) / MS_IN_DAY);
        return Math.round(days + 1);
    }

    private IMetricData findMetricDataByDate(List<IMetricData> dataList, Date date){
        for (IMetricData data: dataList) {
            if (data.getDate().getDate() == date.getDate()){
                return data;
            }
        }
        return null;
    }

    private Date getNormalizedDate(Date date){
        return new Date(date.getYear(), date.getMonth(), date.getDate(),0,0,0);
    }

    private static final String DATE_TO_STR_PATTERN = "MM-dd";

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_TO_STR_PATTERN, Locale.getDefault());

    private String getFormattedDate(Date date)
    {
        return simpleDateFormat.format(date);
    }

    private List<DataPair<Integer>> getDataForNumberMetric(NumberMetric metric, Date startDate, Date endDate) throws SQLException {
        List<NumberMetricData> dataList = numberMetricsRepository.getDataForMetrics(metric, startDate, endDate);

        List<DataPair<Integer>> result = new ArrayList<>();

        Date start = getNormalizedDate(startDate);
        Date end = getNormalizedDate(endDate);

        long daysCount = getDaysDiff(start, end);

        for (int i = 0; i < daysCount; i++) {
            Date currentDate = new Date(start.getTime() + i * MS_IN_DAY);
            NumberMetricData currentData = (NumberMetricData) findMetricDataByDate( new ArrayList<IMetricData>(dataList), currentDate);
            if (currentData == null){
                result.add(new DataPair<>(getFormattedDate(currentDate), 0));
            }
            else {
                result.add(new DataPair<>(getFormattedDate(currentDate), currentData.getValue()));
            }
        }

        return result;
    }
}
