package com.gss.headwaytracker.survey;

public enum SurveyStatusType {
    TEXT_METRICS,
    TIME_METRICS,
    QUANTITATIVE_METRICS,
    RATING_METRICS,
    REPORT
}
