package com.gss.headwaytracker;

import android.annotation.SuppressLint;
import android.support.v7.app.AlertDialog;
import android.app.Activity;
import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;

import com.gss.headwaytracker.preferences_managers.SharedPreferencesManager;

class DialogScreen
{
    static final int IDD_GET_NAME = 1;
    static final int IDD_SEND_ERROR_MESSAGE = 2;

    static AlertDialog getDialog(final Activity activity, int ID) {
        Runnable func = new Runnable() {
            @Override
            public void run() { }
        };

        return switchOnId(activity, func, ID);
    }

    /** Returns an instance of AlertDialog where runnable func is performed after user reaction. */
    static AlertDialog getDialog(final Activity activity, int ID, final Runnable func) {
        return switchOnId(activity, func, ID);
    }

    private static AlertDialog switchOnId(final Activity activity, final Runnable func, int id){
        switch (id) {
            case IDD_GET_NAME:
                return DialogScreen.getGetNameDialog(activity, func);
            case IDD_SEND_ERROR_MESSAGE:
                return DialogScreen.getSendErrorDialog(activity, func);
            default:
                return null;
        }
    }

    private static AlertDialog getSendErrorDialog(final Activity activity, final Runnable func) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity.getApplicationContext());
        builder.setTitle(activity.getString(R.string.app_name))
                .setMessage(activity.getString(R.string.sql_error))
                .setCancelable(false)
                .setNegativeButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        return builder.create();
    }

    /** Returns an instance of GetNameDialog where runnable func is performed after user reaction. */
    private static AlertDialog getGetNameDialog(final Activity activity, final Runnable func) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(R.string.get_name_dialog_title);
        builder.setCancelable(false);

        @SuppressLint("InflateParams")
        final View view = activity.getLayoutInflater().inflate(R.layout.get_name_dialog, null);

        builder.setView(view)
                .setNegativeButton(R.string.cancel_input_name_text,
                        new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String guestText = activity.getResources().getString(R.string.guest_name_text);

                        SharedPreferencesManager.putString(
                                activity.getApplicationContext(),
                                activity.getString(R.string.global_settings_name),
                                activity.getString(R.string.gs_guest_field_name),
                                guestText
                        );

                        func.run();
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.submit_button_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText edit = view.findViewById(R.id.inputName);

                        SharedPreferencesManager.putString(
                                activity.getApplicationContext(),
                                activity.getString(R.string.global_settings_name),
                                activity.getString(R.string.gs_guest_field_name),
                                edit.getText().toString()
                        );

                        func.run();
                    }
                });

        return builder.create();
    }
}
