package com.gss.headwaytracker;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.gss.database.model.Aspect;
import com.gss.database.repositories.implementation.AspectsRepository;
import com.gss.headwaytracker.preferences_managers.GlobalSettingsManager;

import java.util.ArrayList;
import java.util.List;

public class Settings extends AppCompatActivity implements View.OnClickListener {

    private EditText editName;

    private GlobalSettingsManager globalSettingsManager;
    private AspectsRepository aspectsRepository = new AspectsRepository();


    List<Aspect> aspects;
    List<AspectSettingElement> aspectSettingsBlocks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        globalSettingsManager = new GlobalSettingsManager(getApplicationContext());

        editName = findViewById(R.id.editName);
        Button btnChangeName = findViewById(R.id.ChangeNameButton);
        btnChangeName.setOnClickListener(this);

        aspects = aspectsRepository.getAspects(false);
        aspectSettingsBlocks = new ArrayList<>();

        loadUserName();
        generateSettingsView();
    }

    /** Generates path of settings view with aspects and metrics switches */
    private void generateSettingsView() {
        LinearLayout layout = findViewById(R.id.settingsContainer);
        layout.removeAllViews();
        for (Aspect aspect : aspects) {
            AspectSettingElement block = new AspectSettingElement(aspect, getApplicationContext(), layout);
            aspectSettingsBlocks.add(block);
        }
    }


    /** Updates information about aspects and metrics in database. Sets value of field "isActive" in true, if switch is on, sets in false? is switch is off */
    private void saveAspectsMetricsData() {
        for (int i = 0; i < aspectSettingsBlocks.size(); i++) {
            AspectSettingElement block = aspectSettingsBlocks.get(i);
            Aspect aspect = aspects.get(i);

            aspect.setActive(block.isAspectChecked());
            aspectsRepository.addOrUpdateAspect(aspect);

            block.updateMetricsDataBase();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ChangeNameButton:
                saveUserName();
                break;
            default:
                break;
        }
    }

    /** Saves user name to global settings */
    void saveUserName() {
        globalSettingsManager.putGuestName(editName.getText().toString());

        String message = "Приятно познакомиться, ";
        String toastMessage = message.concat(globalSettingsManager.getGuestName());

        Toast toast = Toast.makeText(getApplicationContext(), toastMessage, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    /** Gets user name from global settings and puts it on view */
    void loadUserName() {
        String username = globalSettingsManager.getGuestName();
        editName.setText(username);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveUserName();
        saveAspectsMetricsData();
    }
}
