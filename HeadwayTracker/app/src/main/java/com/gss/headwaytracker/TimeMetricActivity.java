package com.gss.headwaytracker;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.gss.database.model.TimeMetric;
import com.gss.database.model.TimeMetricData;
import com.gss.database.repositories.implementation.TimeMetricsRepository;
import com.gss.headwaytracker.survey.surveyManager.SurveyManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TimeMetricActivity extends AppCompatActivity implements View.OnClickListener {

    private int metricsCounter;
    private List<TimeMetric> timeMetrics;
    private TimeMetricsRepository timeMetricsRepository = new TimeMetricsRepository();

    private boolean isAlreadyTracked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_metric);

        metricsCounter = 0;
        try {
            timeMetrics = timeMetricsRepository.getMetricsForAspect(SurveyManager.getCurrentAspect(), true);
        } catch (SQLException e) {
            sendErrorMessage();
            e.printStackTrace();
        }

        NumberPicker startHourPicker = findViewById(R.id.startHourNumberPicker);
        NumberPicker startMinutePicker = findViewById(R.id.startMinuteNumberPicker);

        startHourPicker.setMinValue(0);
        startHourPicker.setMaxValue(23);
        startMinutePicker.setMinValue(0);
        startMinutePicker.setMaxValue(59);

        NumberPicker endHourPicker = findViewById(R.id.endHourNumberPicker);
        NumberPicker endMinutePicker = findViewById(R.id.endMinuteNumberPicker);

        endHourPicker.setMinValue(0);
        endHourPicker.setMaxValue(23);
        endMinutePicker.setMinValue(0);
        endMinutePicker.setMaxValue(59);

        // That must be after set min/max values of numberPicker
        generateTimeMetricView(timeMetrics.get(metricsCounter));

        Button btnNext = findViewById(R.id.MetricsNextButton);
        Button btnPrev = findViewById(R.id.MetricsPrevButton);
        btnNext.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Date inputFirst = getStartTime();
        Date inputSecond = getEndTime();

        saveData(inputFirst, inputSecond);

        switch (v.getId()) {
            case R.id.MetricsNextButton:
                moveNext();
                break;
            case R.id.MetricsPrevButton:
                movePrev();
                break;
            default:
                break;
        }
    }

    private Date getStartTime() {
        NumberPicker hourPicker = findViewById(R.id.startHourNumberPicker);
        NumberPicker minutePicker = findViewById(R.id.startMinuteNumberPicker);

        Date res = new Date();
        long minutes = hourPicker.getValue() * 60 + minutePicker.getValue();
        long msInMin = 60000; //60 * 1000;
        res.setTime(minutes * msInMin);

        return res;
    }

    private Date getEndTime() {
        NumberPicker hourPicker = findViewById(R.id.endHourNumberPicker);
        NumberPicker minutePicker = findViewById(R.id.endMinuteNumberPicker);

        Date res = new Date();
        long minutes = hourPicker.getValue() * 60 + minutePicker.getValue();
        long msInMin = 60000; //60 * 1000;
        res.setTime(minutes * msInMin);

        return res;
    }

    /**
     * Generates time metric View with name of aspect, name of metric, question and input fields: two fields if metric is interval, one field is metric is not interval
     */
    private void generateTimeMetricView(TimeMetric metric) {
        TextView headerAspect = findViewById(R.id.headerAspectMetricTime);
        headerAspect.setText(metric.getAspect().getName());

        TextView name = findViewById(R.id.nameMetricTime);
        name.setText(metric.getName());

        TextView question = findViewById(R.id.questionMetricTime);
        question.setText(metric.getQuestion());

        LinearLayout timeSecond;
        if (metric.isInterval()) {
            // first input field
            TextView headerTimeFirst = findViewById(R.id.firstTimeHeaderMetricTime);
            headerTimeFirst.setText(getString(R.string.time_start_metric_text));

            // second input field
            TextView headerTimeSecond = findViewById(R.id.secondTimeHeaderMetricTime);
            headerTimeSecond.setVisibility(View.VISIBLE);
            headerTimeSecond.setText(getString(R.string.time_end_metric_text));

            timeSecond = findViewById(R.id.endTimePickerLayout);
            timeSecond.setVisibility(View.VISIBLE);
        }
        else {
            // first input field
            TextView headerTimeFirst = findViewById(R.id.firstTimeHeaderMetricTime);
            headerTimeFirst.setText(getString(R.string.time_metric_text));

            // set invisible second input field
            TextView headerTimeSecond = findViewById(R.id.secondTimeHeaderMetricTime);
            headerTimeSecond.setVisibility(View.INVISIBLE);

            timeSecond = findViewById(R.id.endTimePickerLayout);
            timeSecond.setVisibility(View.INVISIBLE);
        }

        isAlreadyTracked = loadData();
    }

    /** Save answers from current activity to database */
    private void saveData(Date inputFirst, Date inputSecond) {
        TimeMetric metric = timeMetrics.get(metricsCounter);
        TimeMetricData data;

        if (isAlreadyTracked) {
            data = getDataFromDatabase().get(0);
            data.setStartTime(inputFirst);

            if (!metric.isInterval()) {
                data.setEndTime(inputSecond);
            }
            timeMetricsRepository.updateMetricData(data);
        }
        else {
            Date now = new Date();
            Date date = new Date(now.getYear(), now.getMonth(), now.getDate(),0, 0, 0);
            if (metric.isInterval()) {
                data = new TimeMetricData(inputFirst, inputSecond, date, metric);
            }
            else {
                data = new TimeMetricData(inputFirst, null, date, metric);
            }
            timeMetricsRepository.addMetricData(metric, data);
        }
    }

    /** Load answers to current activity from database */
    private boolean loadData() {
        NumberPicker startMin = findViewById(R.id.startMinuteNumberPicker);
        NumberPicker startHour = findViewById(R.id.startHourNumberPicker);
        NumberPicker endMin = findViewById(R.id.endMinuteNumberPicker);
        NumberPicker endHour = findViewById(R.id.endHourNumberPicker);

        List<TimeMetricData> dataList = getDataFromDatabase();
        if (dataList.isEmpty())
            return false;

        TimeMetricData data = dataList.get(0);
        Date startTime = data.getStartTime();
        Date endTime = data.getEndTime();

        int msInHour = 3600000; // 60 * 60 * 1000
        int msInMin = 60000; // 60 * 1000
        int hour = (int) startTime.getTime() / msInHour;
        int minute = ((int) startTime.getTime() % msInHour) / msInMin;
        startHour.setValue(hour);
        startMin.setValue(minute);

        if (endTime != null) {
            hour = (int) endTime.getTime() / msInHour;
            minute = ((int) endTime.getTime() % msInHour) / msInMin;
            endHour.setValue(hour);
            endMin.setValue(minute);
        }

        return true;
    }

    private List<TimeMetricData> getDataFromDatabase() {
        Date now = new Date();
        Date begin = new Date(now.getYear(), now.getMonth(), now.getDate(),0, 0, 0);
        Date end = new Date(now.getYear(), now.getMonth(), now.getDate(),23, 59, 59);

        List<TimeMetricData> dataList = new ArrayList<>();
        try {
            dataList = timeMetricsRepository.getDataForMetrics(timeMetrics.get(metricsCounter), begin, end);
        } catch (SQLException e) {
            sendErrorMessage();
        }
        return dataList;
    }

    private boolean moveNextView(int index) {
        if (index < timeMetrics.size()) {
            generateTimeMetricView(timeMetrics.get(index));
            return true;
        }
        return false;
    }

    private boolean movePrevView(int index) {
        if (index >= 0) {
            generateTimeMetricView(timeMetrics.get(index));
            return true;
        }
        return false;
    }

    private void movePrevActivity(TimeMetricActivity context) {
        SurveyManager.undoStep();
        SurveyManager.moveToNextActivity(this);
    }

    private void moveNextActivity(TimeMetricActivity context) {
        SurveyManager.doStep();
        SurveyManager.moveToNextActivity(this);
    }

    private void moveNext() {
        if (!moveNextView(++metricsCounter))
            moveNextActivity(this);
    }

    private void movePrev() {
        if (!movePrevView(--metricsCounter))
            movePrevActivity(this);
    }

    @Override
    public void onBackPressed() {
        movePrev();
    }

    private void sendErrorMessage() {
        AlertDialog sendErrorDialog = DialogScreen.getDialog(
                this, DialogScreen.IDD_SEND_ERROR_MESSAGE);
        if (sendErrorDialog != null) {
            sendErrorDialog.show();
        }
    }
}
