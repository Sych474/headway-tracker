package com.gss.headwaytracker.survey.surveyManager;

import com.gss.database.model.Aspect;
import com.gss.headwaytracker.survey.SurveyStatusType;

class SurveyElement {

    private SurveyStatusType elementStatusType;

    private Aspect elementAspect;

    public SurveyElement(SurveyStatusType elementStatusType, Aspect elementAspect) {
        this.elementStatusType = elementStatusType;
        this.elementAspect = elementAspect;
    }

    public SurveyStatusType getElementStatusType() {
        return elementStatusType;
    }

    public Aspect getElementAspect() {
        return elementAspect;
    }
}
