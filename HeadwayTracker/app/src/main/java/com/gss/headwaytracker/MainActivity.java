package com.gss.headwaytracker;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.gss.headwaytracker.preferences_managers.GlobalSettingsManager;

public class MainActivity extends AppCompatActivity implements OnClickListener {
    private GlobalSettingsManager globalSettingsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Shared preferences manager
        globalSettingsManager = new GlobalSettingsManager(getApplicationContext());

        // Buttons initialization
        Button btnSettings = findViewById(R.id.SettingsButton);
        Button btnDailyTracking = findViewById(R.id.DailyTrackingButton);
        Button btnStatistics = findViewById(R.id.StatisticsButton);
        btnDailyTracking.setOnClickListener(this);
        btnStatistics.setOnClickListener(this);
        btnSettings.setOnClickListener(this);

        // Checking user name set
        checkUserName();

        // Set start data
        StarterDatabaseFiller filler = new StarterDatabaseFiller(getApplicationContext());
        if (!filler.isFirstInitialized()) {
            filler.prepareMetrics();
            filler.pushDataIntoRepositories();
            filler.prepareMetricsData();
            filler.pushMetricsDataIntoRepositories();
        }
    }

    /** Checks if user name was set. If wasn't, asks for name */
    private void checkUserName() {
        String userName = globalSettingsManager.getGuestName();

        if (userName == null || userName.contentEquals("")) {
            Runnable greetRunnable = new Runnable() {
                @Override
                public void run() {
                    greetUser();
                }
            };

            AlertDialog getNameDialog = DialogScreen.getDialog(
                    this, DialogScreen.IDD_GET_NAME, greetRunnable);
            if (getNameDialog != null) {
                getNameDialog.show();
            }
        } else {
            greetUser();
        }
    }

    /** Show greeting message to user. Can be runnable */
    private void greetUser(){
        String newUserName = globalSettingsManager.getGuestName();
        assert newUserName != null;

        String message = "Добро пожаловать, ";
        String toastMessage = message.concat(newUserName);

        Toast toast = Toast.makeText(getApplicationContext(), toastMessage, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.SettingsButton:
                Intent intent = new Intent(this, Settings.class);
                startActivity(intent);
                break;
            case R.id.DailyTrackingButton:
                Intent intentTracking = new Intent(this, Aspects.class);
                startActivity(intentTracking);
                break;
            case R.id.StatisticsButton:
                sendNotImplementedMessage(v);
                break;
            default:
                break;
        }
    }

    public void sendNotImplementedMessage(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Message!")
                .setMessage("Not implemented now! \n\n P.S. wait next releases")
                .setCancelable(false)
                .setNegativeButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
