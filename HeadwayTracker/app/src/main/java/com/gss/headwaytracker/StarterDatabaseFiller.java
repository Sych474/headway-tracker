package com.gss.headwaytracker;

import android.content.Context;

import com.gss.database.model.Aspect;
import com.gss.database.model.NumberMetric;
import com.gss.database.model.NumberMetricData;
import com.gss.database.model.TextMetric;
import com.gss.database.model.TimeMetric;
import com.gss.database.model.TimeMetricData;
import com.gss.database.repositories.implementation.AspectsRepository;
import com.gss.database.repositories.implementation.NumberMetricsRepository;
import com.gss.database.repositories.implementation.TextMetricsRepository;
import com.gss.database.repositories.implementation.TimeMetricsRepository;
import com.gss.headwaytracker.preferences_managers.GlobalSettingsManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

class StarterDatabaseFiller {
    private GlobalSettingsManager globalSettingsManager;
    private AspectsRepository aspectsRepository;
    private TimeMetricsRepository timeMetricsRepository;
    private TextMetricsRepository textMetricsRepository;
    private NumberMetricsRepository numberMetricsRepository;

    private List<Aspect> aspects;
    private List<TimeMetric> timeMetrics;
    private List<TextMetric> textMetrics;
    private List<NumberMetric> numberMetrics;

    private List<TimeMetricData> timeMetricDataList;
    private List<NumberMetricData> numberMetricDataList;

    private Random random = new Random();

    private Context context;

    StarterDatabaseFiller(Context appContext) {
        context = appContext;
        globalSettingsManager = new GlobalSettingsManager(context);

        // Init repositories
        aspectsRepository = new AspectsRepository();
        timeMetricsRepository = new TimeMetricsRepository();
        textMetricsRepository = new TextMetricsRepository();
        numberMetricsRepository = new NumberMetricsRepository();

        timeMetricDataList = new ArrayList<>();
        numberMetricDataList = new ArrayList<>();
    }

    Boolean isFirstInitialized() {
        return globalSettingsManager.isFirstInitialized();
    }

    void prepareMetrics() {
        // Aspects
        Aspect study = new Aspect("Учеба", true);
        Aspect work = new Aspect("Работа", true);
        Aspect leisure = new Aspect("Досуг", true);
        Aspect sport = new Aspect("Спорт", true);
        Aspect other = new Aspect("Прочее", true);
        Aspect fail = new Aspect("Какой то неактивный", false);

        aspects = new ArrayList<>(Arrays.asList(
                study,
                work,
                leisure,
                sport,
                other,
                fail
        ));

        // Text metrics
        TextMetric goodMoments = new TextMetric(
                "Хорошие моменты дня",
                "Что сегодня хорошего произошло?",
                3,
                leisure,
                true
        );

        TextMetric friendMeets = new TextMetric(
                "Встречи",
                "Кого сегодня видел?",
                2,
                leisure,
                true
        );

        TextMetric workProgress = new TextMetric(
                "Учебная деятельность",
                "Что ты сегодня сделал?",
                3,
                study,
                true
        );

        textMetrics = new ArrayList<>(Arrays.asList(
                goodMoments,
                friendMeets,
                workProgress
        ));

        // Number metrics
        NumberMetric mood = new NumberMetric(
                "Настроение за день",
                "Как настроение?",
                5,
                leisure,
                true,
                true
        );

        NumberMetric workEfficiency = new NumberMetric(
                "Продуктивность работы",
                "Насколько продуктивно сегодня работал?",
                5,
                work,
                true,
                true
        );

        NumberMetric studyEfficiency = new NumberMetric(
                "Продуктивность учебы",
                "Насколько продуктивно сегодня учился?",
                5,
                study,
                true,
                true
        );

        NumberMetric sportEfficiency = new NumberMetric(
                "Продуктивность занятий спортом",
                "Насколько продуктивно сегодня тренировался?",
                5,
                sport,
                true,
                true
        );

        NumberMetric reading = new NumberMetric(
                "Чтение",
                "Cколько страниц ты сегодня прочитал?",
                1000,
                leisure,
                true,
                false
        );

        NumberMetric exercises = new NumberMetric(
                "Упражнения",
                "Cколько упражнений ты сегодня сделал?",
                1000,
                sport,
                true,
                false
        );

        numberMetrics = new ArrayList<>(Arrays.asList(
                mood,
                workEfficiency,
                studyEfficiency,
                sportEfficiency,
                reading,
                exercises
        ));

        // Time metrics
        TimeMetric walkingTime = new TimeMetric(
                "Прогулка",
                "Сколько времени ты сегодня гулял?",
                false,
                leisure,
                true
        );

        TimeMetric internetTime = new TimeMetric(
                "Интернет",
                "Сколько времени ты сегодня провел в интернете (в играх, за сериалами, в социальных сетях)?",
                false,
                leisure,
                true
        );

        TimeMetric sleepingTime = new TimeMetric(
                "Сон",
                "В какое время ты спал?",
                true,
                leisure,
                true
        );

        TimeMetric workTime = new TimeMetric(
                "Время работы",
                "Сколько ты сегодня работал?",
                false,
                work,
                true
        );

        TimeMetric studyTime = new TimeMetric(
                "Время учебы",
                "Сколько ты сегодня учился?",
                false,
                study,
                true
        );

        TimeMetric sportTime = new TimeMetric(
                "Время занятий спортом",
                "Сколько ты сегодня занимался спортом?",
                false,
                sport,
                true
        );

        timeMetrics = new ArrayList<>(Arrays.asList(
                sleepingTime,
                workTime,
                studyTime,
                walkingTime,
                sportTime,
                internetTime
        ));
    }

    /** Generates metrics data for number of days in loop (@i) */
    void prepareMetricsData() {
        Date now = new Date();
        long msInDay = context.getResources().getInteger(R.integer.milliseconds_in_day);
        for (int i = 30; i >= 0; i--) {
            Date currentDate = new Date(now.getTime() - i * msInDay);
            currentDate.setSeconds(0);
            currentDate.setMinutes(0);
            currentDate.setHours(0);
            generateNumberAnswers(currentDate);
            generateRatingAnswers(currentDate);
            generateTimeAnswers(currentDate);
        }
    }

    /** Generates time metrics data for date @currentDate */
    private void generateTimeAnswers(Date currentDate) {
        long minutesInDay = context.getResources().getInteger(R.integer.minutes_in_day);
        long msInMin = context.getResources().getInteger(R.integer.milliseconds_in_min);
        long msInDay = context.getResources().getInteger(R.integer.milliseconds_in_day);

        for (int i = 0; i < timeMetrics.size(); i++) {
            TimeMetric metric = timeMetrics.get(i);
            TimeMetricData timeData;


            if (metric.getName().equals("Сон")) {
                int beginH = getRandomInt(21, 23);
                int beginM = getRandomInt(0, 5);
                int endH = getRandomInt(6, 12);
                int endM = getRandomInt(0, 5);

                Date begin = new Date();
                begin.setHours(beginH);
                begin.setMinutes(beginM*10);

                Date end = new Date();
                end.setTime(end.getTime() + msInDay);
                end.setHours(endH);
                end.setMinutes(endM);

                long minutes = (end.getTime() - begin.getTime())/msInMin;
                minutesInDay -= minutes;
                timeData = new TimeMetricData(begin, end, currentDate, metric);
            }
            else {
                Date time = new Date();
                long minutes = 0;
                switch (metric.getName()) {
                    case "Время учебы":
                        minutes = generateRandomMinutes(15, 540, minutesInDay, 0.9);
                        break;
                    case "Время работы":
                        minutes = generateRandomMinutes(30, 540, minutesInDay, 0.8);
                        break;
                    case "Прогулка":
                        minutes = generateRandomMinutes(15, 240, minutesInDay, 0.4);
                        break;
                    case "Internet":
                        minutes = generateRandomMinutes(10, 300, minutesInDay, 0.9);
                        break;
                    case "Время занятий спортом":
                        minutes = generateRandomMinutes(20, 120, minutesInDay, 0.4);
                        break;
                    default:
                        break;
                }
                minutesInDay -= minutes;
                time.setTime(minutes * msInMin);
                timeData = new TimeMetricData(time, null, currentDate, metric);
            }
            timeMetricDataList.add(timeData);
        }
    }

    /** Generates rating metrics data for date @currentDate */
    private void generateRatingAnswers(Date currentDate) {
        for (int i = 0; i < numberMetrics.size(); i++) {
            NumberMetric metric = numberMetrics.get(i);
            if (metric.isRating()) {
                NumberMetricData ratingData = new NumberMetricData(getRandomIntExponential(1, 5), currentDate, metric);
                numberMetricDataList.add(ratingData);
            }
        }
    }

    /** Generates number metrics data for date @currentDate */
    private void generateNumberAnswers(Date currentDate) {
        for (int i = 0; i < numberMetrics.size(); i++) {
            NumberMetric metric = numberMetrics.get(i);
            if (!metric.isRating()) {
                NumberMetricData numberData = new NumberMetricData(getRandomInt(0, metric.getMaxValue()/2), currentDate, metric);
                numberMetricDataList.add(numberData);
            }
        }
    }

    /** Adds start metrics into database */
    void pushDataIntoRepositories() {
        for (int i = 0; i < aspects.size(); i++) {
            aspectsRepository.addOrUpdateAspect(aspects.get(i));
        }

        for (int i = 0; i < timeMetrics.size(); i++) {
            timeMetricsRepository.addOrUpdateMetric(timeMetrics.get(i));
        }

        for (int i = 0; i < textMetrics.size(); i++) {
            textMetricsRepository.addOrUpdateMetric(textMetrics.get(i));
        }

        for (int i = 0; i < numberMetrics.size(); i++) {
            numberMetricsRepository.addOrUpdateMetric(numberMetrics.get(i));
        }

        globalSettingsManager.putFirstInitState(true);
    }

    /** Adds start metrics data into database */
    void pushMetricsDataIntoRepositories() {
        for (int i = 0; i < timeMetricDataList.size(); i++) {
            TimeMetricData metricData = timeMetricDataList.get(i);
            timeMetricsRepository.addMetricData(metricData.getMetric(), metricData);
        }

        for (int i = 0; i < numberMetricDataList.size(); i++) {
            NumberMetricData metricData = numberMetricDataList.get(i);
            numberMetricsRepository.addMetricData(metricData.getMetric(), metricData);
        }
    }

    /** Returns random integer from @min to @max (included) with exponential distribution */
    private int getRandomIntExponential(int min, int max) {
        double lambda = 3;
        double randNum = 1 - Math.exp(- lambda * random.nextDouble());
        return (int) Math.round(randNum * (max-min) + min);
    }

    /** Returns random integer from @min to @max (included) with uniform distribution */
    private int getRandomInt(int min, int max) {
        return random.nextInt(max-min) + min;
    }

    /** Returns random number from @min to @max if number less than @minutesLeft
     * or 0 with probability = @probability */
    private long generateRandomMinutes(int min, int max, long minutesLeft, double probability) {
        double rand = random.nextDouble();
        if (rand > probability)
            return 0;
        else {
            long r = random.nextInt(max-min) + min;
            return r <= minutesLeft ? r : minutesLeft;
        }
    }
}
