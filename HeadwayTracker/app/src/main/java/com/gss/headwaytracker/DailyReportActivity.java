package com.gss.headwaytracker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gss.database.model.Aspect;
import com.gss.database.model.NumberMetric;
import com.gss.database.model.NumberMetricData;
import com.gss.database.model.TimeMetric;
import com.gss.database.model.TimeMetricData;
import com.gss.database.repositories.implementation.NumberMetricsRepository;
import com.gss.database.repositories.implementation.TimeMetricsRepository;
import com.gss.headwaytracker.survey.surveyManager.SurveyManager;
import com.j256.ormlite.stmt.query.In;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DailyReportActivity extends AppCompatActivity implements View.OnClickListener {

    private NumberMetricsRepository numberMetricsRepository = new NumberMetricsRepository();
    private TimeMetricsRepository timeMetricsRepository = new TimeMetricsRepository();

    List<Aspect> activeAspects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_report);

        Button btnSubmit = (Button) findViewById(R.id.submitButton);
        btnSubmit.setOnClickListener(this);

        activeAspects = SurveyManager.GetAspects();
        generateMainText();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitButton:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    public static Date getStartOfToday(Context context) {
        // Set dates
        Date startOfToday = new Date();

        int ms_in_day = context.getResources().getInteger(R.integer.milliseconds_in_day);
        long offset = startOfToday.getTime() % ms_in_day;

        startOfToday.setTime(startOfToday.getTime() - offset);

        return startOfToday;
    }

    public static Date getCurrentDate() {
        return new Date();
    }

    public static List<Integer> getColorList(int size) {
        List<Integer> colors = new ArrayList<>();

        int[] rgb = new int[3];
        int switcher = 0;

        for (int i = 1; i < size+1; i++) {
            rgb[switcher] += i * 100;
            rgb[switcher] %= 255;
            switcher = i % 3;

            colors.add(Color.rgb(rgb[0], rgb[1], rgb[2]));
        }

        return colors;
    }

    private void generateMainText() {
        String aspectMessage = "Сегодня ты отметил:\n";
        List<String> aspectNames = new ArrayList<String>();

        for (Aspect aspect : activeAspects) {
            aspectNames.add("\t- " + aspect.getName() + "\n");
        }

        TextView mainTextView = (TextView) findViewById(R.id.mainTextView);
        StringBuilder builder = new StringBuilder(aspectMessage);
        for (String aspectName : aspectNames) {
            builder.append(aspectName);
        }

        builder.append("\n")
                .append(getRatingComparisonText())
                .append("\n")
                .append(renderTimePieChart());

        mainTextView.setText(builder.toString());
    }

    private String getRatingComparisonText() {
        List<NumberMetric> metricsForToday = getAllRatingMetricsForToday(activeAspects);
        if (metricsForToday.isEmpty()) {
            return "Сегодня не было ни одной оценки от тебя.";
        }

        NumberMetric bestMetric = metricsForToday.get(0);
        int bestValue = getLastDataForNumberMetric(bestMetric).getValue();
        NumberMetric worstMetric = metricsForToday.get(0);
        int worstValue = getLastDataForNumberMetric(worstMetric).getValue();

        for (int i = 1; i < metricsForToday.size(); i++) {
            NumberMetricData currentMetricData = getLastDataForNumberMetric(metricsForToday.get(i));
            NumberMetricData bestMetricData = getLastDataForNumberMetric(bestMetric);
            NumberMetricData worstMetricData = getLastDataForNumberMetric(worstMetric);

            if (currentMetricData.getValue() > bestMetricData.getValue()) {
                bestMetric = metricsForToday.get(i);
                bestValue = currentMetricData.getValue();
            }

            if (currentMetricData.getValue() < worstMetricData.getValue()) {
                worstMetric = metricsForToday.get(i);
                worstValue = currentMetricData.getValue();
            }
        }

        return ("Лучшая оценка на сегодня: " +
                bestValue +
                " (" +
                bestMetric.getName() +
                ")\n" +
                "Худшая оценка на сегодня: " +
                worstValue +
                " (" +
                worstMetric.getName() +
                ")\n");
    }

    @SuppressLint("DefaultLocale")
    private String renderTimePieChart() {
        StringBuilder builder = new StringBuilder("Сегодня ты потратил:\n");
        PieChart mPieChart = findViewById(R.id.pieChart);
        List<Integer> colors = getColorList(activeAspects.size());

        for (int i = 0; i < activeAspects.size(); i++) {
            Aspect aspect = activeAspects.get(i);
            long allTime = getSumTimeForAspectForToday(aspect);

            mPieChart.addPieSlice(new PieModel(aspect.getName(), allTime, colors.get(i)));

            builder.append(aspect.getName())
                    .append(": ")
                    .append(String.format("%d часов и %d минут", allTime / 60, allTime % 60))
                    .append("\n");
        }

        mPieChart.setInnerValueUnit("min");
        mPieChart.startAnimation();

        return builder.toString();
    }

    /** Returns sum of time spent on given activity in minutes*/
    private long getSumTimeForAspectForToday(Aspect aspect) {
        List<TimeMetric> metrics = getAllTimeMetricsForAspectForToday(aspect);
        if (metrics == null)
            return 0;

        long res = 0;

        for (int i = 0; i < metrics.size(); i++) {
            List<TimeMetricData> data;
            try {
                data = timeMetricsRepository.getDataForMetrics(
                        metrics.get(i), getStartOfToday(getApplicationContext()), getCurrentDate()
                );
            }
            catch (SQLException e) {
                data = new ArrayList<>();
            }

            long sum = 0;
            for (int j = 0; j < data.size(); j++) {
                sum += data.get(j).getStartTime().getTime();
            }

            res += sum;
        }

        long ms = 60000; // 60 * 1000

        return res / ms;
    }

    private List<TimeMetric> getAllTimeMetricsForAspectForToday(Aspect aspect) {
        List<TimeMetric> timeMetrics;
        try {
            timeMetrics = timeMetricsRepository.getMetricsForAspect(aspect, false);
        } catch (SQLException e) {
            return null;
        }

        if (timeMetrics.isEmpty())
            return null;

        List<TimeMetric> resMetrics = new ArrayList<>();
        for (int i = 0; i < timeMetrics.size(); i++) {
            if (hasTimeMetricDataForToday(timeMetrics.get(i))){
                resMetrics.add(timeMetrics.get(i));
            }
        }

        return resMetrics;
    }

    /** Returns true when metric has data for today */
    private boolean hasTimeMetricDataForToday(TimeMetric metric) {
        try {
            List<TimeMetricData> timeMetricData = timeMetricsRepository.getDataForMetrics(
                    metric, getStartOfToday(getApplicationContext()), getCurrentDate()
            );
            return timeMetricData.size() > 0;
        }
        catch (SQLException e) {
            return false;
        }
    }

    /** Returns a list of rating metrics for given aspects for today */
    private List<NumberMetric> getAllRatingMetricsForToday(List<Aspect> aspects) {
        List<NumberMetric> res = new ArrayList<>();

        for (int i = 0; i < aspects.size(); i++) {
            List<NumberMetric> currentList = getAllRatingMetricsForAspectForToday(aspects.get(i));
            if (currentList != null){
                res.addAll(currentList);
            }
        }

        return res;
    }

    /** Returns a list of rating metrics for given aspect for today */
    private List<NumberMetric> getAllRatingMetricsForAspectForToday(Aspect aspect) {
        List<NumberMetric> numberMetrics;
        try {
            numberMetrics = numberMetricsRepository.getRatingMetrics(aspect, false);
        }
        catch (SQLException e) {
            return null;
        }

        if (numberMetrics.isEmpty())
            return null;

        List<NumberMetric> resMetrics = new ArrayList<>();
        for (int i = 0; i < numberMetrics.size(); i++) {
            if (hasNumberMetricDataForToday(numberMetrics.get(i))){
                resMetrics.add(numberMetrics.get(i));
            }
        }

        return resMetrics;
    }

    /** Returns true when metric has data for today */
    private boolean hasNumberMetricDataForToday(NumberMetric metric) {
        try {
            List<NumberMetricData> numberMetricData = numberMetricsRepository.getDataForMetrics(
                    metric, getStartOfToday(getApplicationContext()), getCurrentDate()
            );
            return numberMetricData.size() > 0;
        }
        catch (SQLException e) {
            return false;
        }
    }

    /** Returns the last answer for given number metric */
    private NumberMetricData getLastDataForNumberMetric(NumberMetric metric) {
        List<NumberMetricData> dataList = numberMetricsRepository.getAllDataForMetrics(metric);

        NumberMetricData lastData = dataList.get(0);
        for (int i = 0; i < dataList.size(); i++) {
            NumberMetricData data = dataList.get(i);

            if (data.getDate().getTime() > lastData.getDate().getTime())
                lastData = dataList.get(i);
        }

        return lastData;
    }

}
