package com.gss.headwaytracker;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gss.database.model.Aspect;
import com.gss.database.repositories.implementation.AspectsRepository;
import com.gss.database.repositories.implementation.NumberMetricsRepository;
import com.gss.database.repositories.implementation.TextMetricsRepository;
import com.gss.database.repositories.implementation.TimeMetricsRepository;
import com.gss.headwaytracker.survey.surveyManager.SurveyManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Aspects extends AppCompatActivity implements View.OnClickListener {
    final String username = "Guest";

    private TextView helloMessage;
    private Button btnNext;
    private LinearLayout container;
    private List<CheckBox> checkBoxes;

    private AspectsRepository aspectsRepository = new AspectsRepository();
    private NumberMetricsRepository numberMetricsRepository = new NumberMetricsRepository();
    private TimeMetricsRepository timeMetricsRepository = new TimeMetricsRepository();
    private TextMetricsRepository textMetricsRepository = new TextMetricsRepository();

    List<Aspect> activeAspects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aspects);

        helloMessage = findViewById(R.id.aspectHelloMessage);
        helloMessage.setText(getHelloString());

        container = findViewById(R.id.aspectContainer);

        checkBoxes = generateAspectsSelection(container);

        btnNext = findViewById(R.id.aspectNextButton);
        btnNext.setOnClickListener(this);
    }

    private List<CheckBox> generateAspectsSelection(LinearLayout container) {
        List<Aspect> aspects = aspectsRepository.getAspects(false);
        container.removeAllViews();

        List<CheckBox> checkBoxes = new ArrayList<>();
        for (Aspect aspect : aspects) {
            try {
                int metricsCount = numberMetricsRepository.getMetricsForAspect(aspect, false).size() +
                                    timeMetricsRepository.getMetricsForAspect(aspect, false).size() +
                                    textMetricsRepository.getMetricsForAspect(aspect, false).size();
                if (aspect.isActive() && metricsCount > 0) {
                    CheckBox checkBox = new CheckBox(getApplicationContext());
                    checkBox.setText(aspect.getName());
                    checkBox.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    checkBox.setTextColor(Color.DKGRAY);
                    checkBox.setButtonTintList(ColorStateList.valueOf(0xFF008577));
                    container.addView(checkBox);

                    checkBoxes.add(checkBox);
                }
                else {
                    CheckBox checkBox = new CheckBox(getApplicationContext());
                    checkBox.setChecked(false);
                    checkBoxes.add(checkBox);
            }
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return checkBoxes;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.aspectNextButton:
                activeAspects = getInputActiveAspects();

                if (!SurveyManager.init(activeAspects)) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }
                else {
                    SurveyManager.moveToNextActivity(this);
                }
                break;
            default:
                break;
        }
    }


    private String getHelloString() {
        SharedPreferences sharedPreferences = getSharedPreferences("global_settings", MODE_PRIVATE);
        String username = sharedPreferences.getString(this.username, "");

        return getString(R.string.hello) + ", " + username;
    }

    private List<Aspect> getInputActiveAspects() {
        List<Aspect> aspects = aspectsRepository.getAspects(false);

        List<Aspect> activeAspects = new ArrayList<>();
        for (int i = 0; i < aspects.size(); i++) {
            if (checkBoxes.get(i).isChecked()) {
                activeAspects.add(aspects.get(i));
            }
        }
        return activeAspects;
    }
}
