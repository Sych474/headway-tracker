package com.gss.headwaytracker;

import android.content.Context;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public final class Instrumentation {

    /** Converts List of TextEdit Objects to List of Strings */
    public static List<String> convertTextEditsToStringList(List<EditText> edits) {
        List<String> strings = new ArrayList<>();

        for (EditText text : edits) {
            strings.add(text.getText().toString());
        }

        return strings;
    }

    /** Converts List on Strings to List of TextEdit Objects */
    public static List<EditText> convertTextEditsToStringList(List<String> strings, Context context) {
        List<EditText> edits = new ArrayList<>();

        for (String string : strings) {
            EditText edit = new EditText(context);
            edit.setText(string);

            edits.add(edit);
        }

        return edits;
    }

}

