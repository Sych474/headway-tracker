package com.gss.headwaytracker;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.gss.database.HelperFactory;
import com.gss.database.model.Aspect;
import com.gss.database.model.NumberMetric;
import com.gss.database.model.NumberMetricData;
import com.gss.database.repositories.implementation.AspectsRepository;
import com.gss.database.repositories.implementation.NumberMetricsRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class NumberMetricsRepositoryTest {
    private NumberMetricsRepository repository;
    private AspectsRepository aspectsRepository;

    private Aspect aspect_1;
    private Aspect aspect_2;

    // Text metrics
    private NumberMetric ratingActiveMetric;
    private NumberMetric numberActiveMetric;
    private NumberMetric ratingNotActiveMetric;
    private NumberMetric numberNotActiveMetric;

    // Text metric data
    private NumberMetricData testMetricData_1;
    private NumberMetricData testMetricData_2;

    @Before
    public void setUp() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        HelperFactory.setHelper(appContext);

        // Create repository
        repository = new NumberMetricsRepository();
        aspectsRepository = new AspectsRepository();

        // Create aspect_1
        aspect_1 = new Aspect();
        buildAspect(aspect_1, 1);
        aspect_2 = new Aspect();
        buildAspect(aspect_2, 2);

        // Create test metrics
        ratingActiveMetric = new NumberMetric();
        buildTestMetric(ratingActiveMetric, aspect_1, 1, true, true);
        numberActiveMetric = new NumberMetric();
        buildTestMetric(numberActiveMetric, aspect_1, 2, false, true);
        ratingNotActiveMetric = new NumberMetric();
        buildTestMetric(ratingNotActiveMetric, aspect_2, 3, true, false);
        numberNotActiveMetric = new NumberMetric();
        buildTestMetric(numberNotActiveMetric, aspect_2, 4, false, false);


        testMetricData_1 = new NumberMetricData();
        buildTestMetricData(testMetricData_1, 1, ratingActiveMetric);
        testMetricData_2 = new NumberMetricData();
        buildTestMetricData(testMetricData_2, 2, numberActiveMetric);

        flushRepository();
        flushAspectRepository();
        aspectsRepository.addOrUpdateAspect(aspect_1);
        aspectsRepository.addOrUpdateAspect(aspect_2);
    }

    @Test
    public void getAllRatingMetricsTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<NumberMetric> metrics = repository.getRatingMetrics(null, false);

        // Assert
        assertEquals(2, metrics.size());
        assertMetrics(ratingActiveMetric, metrics.get(0));
        assertMetrics(ratingNotActiveMetric, metrics.get(1));
    }

    @Test
    public void getAсешмуRatingMetricsTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<NumberMetric> metrics = repository.getRatingMetrics(null, true);

        // Assert
        assertEquals(1, metrics.size());
        assertMetrics(ratingActiveMetric, metrics.get(0));
    }

    @Test
    public void getAllQuantitativeMetricsTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<NumberMetric> metrics = repository.getQuantitativeMetrics(null, false);

        // Assert
        assertEquals(2, metrics.size());
        assertMetrics(numberActiveMetric, metrics.get(0));
        assertMetrics(numberNotActiveMetric, metrics.get(1));
    }

    @Test
    public void getActiveQuantitativeMetricsTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<NumberMetric> metrics = repository.getQuantitativeMetrics(null, true);

        // Assert
        assertEquals(1, metrics.size());
        assertMetrics(numberActiveMetric, metrics.get(0));
    }

    @Test
    public void getAllQuantitativeMetricsForAspectTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<NumberMetric> metrics = repository.getQuantitativeMetrics(aspect_1, false);

        // Assert
        assertEquals(1, metrics.size());
        assertMetrics(numberActiveMetric, metrics.get(0));
    }

    @Test
    public void getActiveQuantitativeMetricsForAspectTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<NumberMetric> activeMetrics = repository.getQuantitativeMetrics(aspect_1, true);
        List<NumberMetric> otherMetrics = repository.getQuantitativeMetrics(aspect_2, true);

        // Assert
        assertEquals(1, activeMetrics.size());
        assertMetrics(numberActiveMetric, activeMetrics.get(0));
        assertEquals(0, otherMetrics.size());
    }

    @Test
    public void getAllRatingMetricsForAspectTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<NumberMetric> metrics = repository.getRatingMetrics(aspect_1, false);

        // Assert
        assertEquals(1, metrics.size());
        assertMetrics(ratingActiveMetric, metrics.get(0));
    }

    @Test
    public void getActiveRatingMetricsForAspectTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<NumberMetric> activeMetrics = repository.getRatingMetrics(aspect_1, true);
        List<NumberMetric> otherMetrics = repository.getRatingMetrics(aspect_2, true);

        // Assert
        assertEquals(1, activeMetrics.size());
        assertMetrics(ratingActiveMetric, activeMetrics.get(0));
        assertEquals(0, otherMetrics.size());
    }

    @Test
    public void getAllMetricsTest() {
        // Arrange
        addAllMetrics();

        // Act
        List<NumberMetric> metrics = repository.getMetrics(false);

        // Assert
        assertEquals(4, metrics.size());
        assertMetrics(ratingActiveMetric, metrics.get(0));
        assertMetrics(numberActiveMetric, metrics.get(1));
        assertMetrics(ratingNotActiveMetric, metrics.get(2));
        assertMetrics(numberNotActiveMetric, metrics.get(3));
    }

    @Test
    public void getAllMetricsForAspectTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<NumberMetric> metrics = repository.getMetricsForAspect(aspect_1, false);

        // Assert
        assertEquals(2, metrics.size());
        assertMetrics(ratingActiveMetric, metrics.get(0));
        assertMetrics(numberActiveMetric, metrics.get(1));
    }

    @Test
    public void addMetricTest() {
        // Arrange
        List<NumberMetric> listOfMetric;

        // Act
        repository.addOrUpdateMetric(ratingActiveMetric);

        // Assert
        listOfMetric = repository.getMetrics(false);
        assertEquals(1, listOfMetric.size());
        NumberMetric actualMetric = listOfMetric.get(0);
        assertMetrics(ratingActiveMetric, actualMetric);
    }

    @Test
    public void updateMetricTest() {
        // Arrange
        List<NumberMetric> listOfMetric;
        repository.addOrUpdateMetric(ratingActiveMetric);

        ratingActiveMetric.setName(numberActiveMetric.getName());
        ratingActiveMetric.setActive(numberActiveMetric.isActive());
        ratingActiveMetric.setQuestion(numberActiveMetric.getQuestion());
        ratingActiveMetric.setAspect(numberActiveMetric.getAspect());

        // Act
        repository.addOrUpdateMetric(ratingActiveMetric);

        // Assert
        listOfMetric = repository.getMetrics(false);
        assertEquals(1, listOfMetric.size());
        NumberMetric actualMetric = listOfMetric.get(0);
        assertMetrics(ratingActiveMetric, actualMetric);
    }

    @Test
    public void findMetricsByNameTest() {
        // Arrange
        repository.addOrUpdateMetric(ratingActiveMetric);
        repository.addOrUpdateMetric(numberActiveMetric);

        // Act
        NumberMetric metricFound = repository.findMetricsByName(ratingActiveMetric.getName());

        // Assert
        assertMetrics(ratingActiveMetric, metricFound);
    }

    @Test
    public void findMetricsByNameNotFoundTest() {
        // Act
        NumberMetric metricFound = repository.findMetricsByName(numberActiveMetric.getName());

        // Assert
        assertNull(metricFound);
    }

    @Test
    public void removeMetricTest() {
        // Arrange
        assertEquals( 0, repository.getMetrics(false).size());
        repository.addOrUpdateMetric(ratingActiveMetric);
        repository.addOrUpdateMetric(numberActiveMetric);

        // Act
        repository.removeMetric(numberActiveMetric);

        // Assert
        NumberMetric actualMetric = repository.findMetricsByName(ratingActiveMetric.getName());
        assertMetrics(ratingActiveMetric, actualMetric);
    }

    @Test
    public void removeMetricEmptyTest() {
        // Arrange
        assertEquals( 0, repository.getMetrics(false).size());

        // Act
        repository.removeMetric(ratingActiveMetric);

        // Assert
        assertEquals( 0, repository.getMetrics(false).size());
    }

    @Test
    public void addMetricDataTest() {
        // Arrange
        repository.addOrUpdateMetric(ratingActiveMetric);

        // Act
        NumberMetricData result = repository.addMetricData(ratingActiveMetric, testMetricData_1);

        // Assert
        assertMetricsData(testMetricData_1, result);

        List<NumberMetricData> resList = repository.getAllDataForMetrics(ratingActiveMetric);
        assertMetricsData(testMetricData_1, resList.get(0));
        assertEquals(1, resList.size());
    }

    @Test
    public void addMetricDataNullMetricTest() {
        // Act
        NumberMetricData result = repository.addMetricData(null, testMetricData_1);

        // Assert
        assertNull(result);
    }

    @Test
    public void updateMetricDataTest() {
        // Arrange
        repository.addOrUpdateMetric(ratingActiveMetric);
        repository.addMetricData(ratingActiveMetric, testMetricData_1);

        testMetricData_1.setValue(100);
        Date newDate = new Date();
        newDate.setTime(1000);
        testMetricData_1.setDate(newDate);

        // Act
        repository.updateMetricData(testMetricData_1);

        // Assert
        List<NumberMetricData> resList = repository.getAllDataForMetrics(ratingActiveMetric);
        assertEquals(1, resList.size());
        assertMetricsData(testMetricData_1, resList.get(0));
    }

    @Test
    public void updateMetricDataEmptyTest() {
        // Arrange
        repository.addOrUpdateMetric(ratingActiveMetric);
        repository.addMetricData(ratingActiveMetric, testMetricData_2);

        // Act
        repository.updateMetricData(testMetricData_1);

        // Assert
        List<NumberMetricData> resList = repository.getAllDataForMetrics(ratingActiveMetric);
        assertEquals(1, resList.size());
        assertMetricsData(testMetricData_2, resList.get(0));
    }

    @Test
    public void removeMetricDataTest() {
        // Arrange
        repository.addOrUpdateMetric(ratingActiveMetric);
        repository.addMetricData(ratingActiveMetric, testMetricData_1);
        repository.addMetricData(ratingActiveMetric, testMetricData_2);

        // Act
        repository.removeMetricData(testMetricData_1);

        // Assert
        List<NumberMetricData> resList = repository.getAllDataForMetrics(ratingActiveMetric);
        assertMetricsData(testMetricData_2, resList.get(0));
        assertEquals(1, resList.size());
    }

    @Test
    public void getAllDataForMetricsTest() {
        // Arrange
        repository.addOrUpdateMetric(ratingActiveMetric);
        repository.addMetricData(ratingActiveMetric, testMetricData_1);
        repository.addMetricData(ratingActiveMetric, testMetricData_2);

        // Act
        List<NumberMetricData> metricsData = repository.getAllDataForMetrics(ratingActiveMetric);

        // Assert
        assertEquals(2, metricsData.size());
        assertMetricsData(testMetricData_1, metricsData.get(0));
        assertMetricsData(testMetricData_2, metricsData.get(1));
    }

    @Test
    public void getDataForMetricsTest() throws SQLException {
        // Arrange
        NumberMetricData testMetricData_3 = new NumberMetricData();
        buildTestMetricData(testMetricData_3, 3, ratingActiveMetric);

        NumberMetricData testMetricData_4 = new NumberMetricData();
        buildTestMetricData(testMetricData_4, 4, ratingActiveMetric);

        repository.addOrUpdateMetric(ratingActiveMetric);
        repository.addMetricData(ratingActiveMetric, testMetricData_1);
        repository.addMetricData(ratingActiveMetric, testMetricData_2);
        repository.addMetricData(ratingActiveMetric, testMetricData_3);
        repository.addMetricData(ratingActiveMetric, testMetricData_4);

        // Act
        List<NumberMetricData> resList = repository.getDataForMetrics(ratingActiveMetric, testMetricData_2.getDate(), testMetricData_3.getDate());

        // Assert
        assertEquals(2, resList.size());
        assertMetricsData(testMetricData_2, resList.get(0));
        assertMetricsData(testMetricData_3, resList.get(1));
    }

    @Test
    public void getDataForMetricsErrorTest() {
        // Arrange
        boolean isFailed = false;

        // Act
        try {
            repository.getDataForMetrics(null, testMetricData_2.getDate(), testMetricData_1.getDate());
        }
        catch (SQLException exception) {
            isFailed = true;
        }

        // Assert
        assertTrue(isFailed);
    }

    @Test
    public void getDataForMetricsEmptyTest() throws SQLException {
        // Arrange
        repository.addOrUpdateMetric(ratingActiveMetric);
        repository.addMetricData(ratingActiveMetric, testMetricData_1);
        repository.addMetricData(ratingActiveMetric, testMetricData_2);

        // Act
        List<NumberMetricData> resList = repository.getDataForMetrics(ratingActiveMetric, testMetricData_2.getDate(), testMetricData_1.getDate());

        // Assert
        assertTrue(resList.isEmpty());
    }

    @Test
    public void removeAllMetricsDataTest() {
        // Arrange
        repository.addOrUpdateMetric(ratingActiveMetric);
        repository.addMetricData(ratingActiveMetric, testMetricData_1);
        repository.addMetricData(ratingActiveMetric, testMetricData_2);

        // Act
        try {
            repository.removeAllMetricsData(ratingActiveMetric);
        }
        catch (SQLException exception) {
            fail();
        }

        List<NumberMetricData> metricsData = repository.getAllDataForMetrics(ratingActiveMetric);

        // Assert
        assertEquals(0, metricsData.size());
    }

    @After
    public void releaseAll() {
        HelperFactory.releaseHelper();

        repository = null;
        aspect_1 = null;
        aspectsRepository = null;
        ratingActiveMetric = null;
        numberActiveMetric = null;
        testMetricData_1 = null;
        testMetricData_2 = null;
    }

    private void buildAspect(Aspect aspect, int i) {
        String index = Integer.toString(i);

        aspect.setActive(true);
        aspect.setName("test_aspect_" + index);
    }

    /** Sets test TextMetric by test values */
    private void buildTestMetric(NumberMetric metric, Aspect aspect, int i, boolean rating, boolean isActive) {
        String index = Integer.toString(i);

        metric.setName("test_metric_name_" + index);
        metric.setQuestion("question_" + index);
        metric.setActive(isActive);
        metric.setMaxValue(i*2);
        metric.setRating(rating);
        metric.setAspect(aspect);
    }

    /** Sets test TextMetricData by test values */
    private void buildTestMetricData(NumberMetricData data, int i, NumberMetric metric) {
        Date date = new Date();
        date.setTime(i);

        data.setDate(date);
        data.setValue(i);
        data.setMetric(metric);
    }

    private void assertAspects(Aspect expected, Aspect actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.isActive(), actual.isActive());
    }

    /** Asserts expected and actual text metrics */
    private void assertMetrics(NumberMetric expected, NumberMetric actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getMaxValue(), actual.getMaxValue());
        assertEquals(expected.isRating(), actual.isRating());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getQuestion(), actual.getQuestion());
        assertEquals(expected.isActive(), actual.isActive());
        assertAspects(expected.getAspect(), actual.getAspect());
    }

    /** Asserts expected and actual text metrics data*/
    private void assertMetricsData(NumberMetricData expected, NumberMetricData actual) {
        assertEquals(expected.getDate(), actual.getDate());
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getValue(), actual.getValue());
        assertMetrics(expected.getMetric(), actual.getMetric());
    }

    private void flushRepository() {
        List<NumberMetric> listOfMetric = repository.getMetrics(false);
        for (int i = 0; i < listOfMetric.size(); i++) {
            try {
                repository.removeAllMetricsData(listOfMetric.get(i));
            }
            catch (SQLException exception) {
                fail();
            }
            repository.removeMetric(listOfMetric.get(i));
        }
    }

    private void flushAspectRepository() {
        List<Aspect> aspects = aspectsRepository.getAspects(false);
        for (int i = 0; i < aspects.size(); i++) {
            aspectsRepository.removeAspect(aspects.get(i));
        }
    }

    private void  addAllMetrics() {
        repository.addOrUpdateMetric(ratingActiveMetric);
        repository.addOrUpdateMetric(numberActiveMetric);
        repository.addOrUpdateMetric(ratingNotActiveMetric);
        repository.addOrUpdateMetric(numberNotActiveMetric);
    }
}
