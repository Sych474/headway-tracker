package com.gss.headwaytracker;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.gss.database.HelperFactory;
import com.gss.database.model.Aspect;
import com.gss.database.repositories.implementation.AspectsRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class AspectsRepositoryTest {
    private AspectsRepository repository;

    private Aspect activeAspect;
    private Aspect notActiveAspect;

    @Before
    public void setUp() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        HelperFactory.setHelper(appContext);

        // Create repository
        repository = new AspectsRepository();

        // Create testAspects
        activeAspect = new Aspect();
        buildAspect(activeAspect, 1, true);

        notActiveAspect = new Aspect();
        buildAspect(notActiveAspect, 2, false);

        // Flush repository
        List<Aspect> aspects = repository.getAspects(false);
        for (int i = 0; i < aspects.size(); i++) {
            repository.removeAspect(aspects.get(i));
        }
    }

    @Test
    public void getAllAspectsTest() {
        // Arrange
        addAllAspects();

        // Act
        List<Aspect> aspects = repository.getAspects(false);

        // Assert
        assertEquals(2, aspects.size());
        assertAspects(activeAspect, aspects.get(0));
        assertAspects(notActiveAspect, aspects.get(1));
    }

    @Test
    public void getAllActiveAspectsTest() {
        // Arrange
        addAllAspects();

        // Act
        List<Aspect> aspects = repository.getAspects(true);

        // Assert
        assertEquals(1, aspects.size());
        assertAspects(activeAspect, aspects.get(0));
    }

    @Test
    public void addAspectTest() {
        // Arrange
        List<Aspect> aspects = repository.getAspects(false);
        assertTrue(aspects.isEmpty());

        // Act
        repository.addOrUpdateAspect(activeAspect);

        // Assert
        aspects = repository.getAspects(false);
        assertEquals(1, aspects.size());
        assertAspects(activeAspect, aspects.get(0));
    }

    @Test
    public void updateAspectTest() {
        // Arrange
        List<Aspect> aspects;
        repository.addOrUpdateAspect(activeAspect);
        activeAspect.setName(notActiveAspect.getName());

        // Act
        repository.addOrUpdateAspect(activeAspect);

        // Assert
        aspects = repository.getAspects(false);
        assertEquals(1, aspects.size());
        assertAspects(activeAspect, aspects.get(0));
    }

    @Test
    public void removeAspectTest() {
        // Arrange
        addAllAspects();

        // Act
        repository.removeAspect(notActiveAspect);

        // Assert
        List<Aspect> aspects = repository.getAspects(false);
        assertEquals(1, aspects.size());
        assertAspects(activeAspect, aspects.get(0));
    }

    @Test
    public void removeAspectEmptyTest() {
        // Arrange
        assertEquals(0, repository.getAspects(false).size());

        // Act
        repository.removeAspect(activeAspect);

        // Assert
        assertEquals(0, repository.getAspects(false).size());
    }


    @After
    public void releaseAll() {
        HelperFactory.releaseHelper();

        repository = null;
        activeAspect = null;
        notActiveAspect = null;
    }

    private void buildAspect(Aspect aspect, int i, boolean isActive) {
        String index = Integer.toString(i);

        aspect.setActive(isActive);
        aspect.setName("test_aspect_" + index);
    }

    private void assertAspects(Aspect expected, Aspect actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.isActive(), actual.isActive());
    }

    private void addAllAspects(){
        repository.addOrUpdateAspect(activeAspect);
        repository.addOrUpdateAspect(notActiveAspect);
    }
}
