package com.gss.headwaytracker;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.gss.database.HelperFactory;
import com.gss.database.model.Aspect;
import com.gss.database.model.TimeMetric;
import com.gss.database.model.TimeMetricData;
import com.gss.database.repositories.implementation.AspectsRepository;
import com.gss.database.repositories.implementation.TimeMetricsRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class TimeMetricsRepositoryTest {
    private TimeMetricsRepository repository;
    private AspectsRepository aspectsRepository;

    private Aspect aspect_1;
    private Aspect aspect_2;

    // Text metrics
    private TimeMetric activeTimeMetric;
    private TimeMetric notActiveTimeMetric;
    private TimeMetric activeIntervalMetric;
    private TimeMetric notActiveIntervalMetric;


    // Text metric data
    private TimeMetricData testMetricData_1;
    private TimeMetricData testMetricData_2;

    @Before
    public void setUp() throws SQLException {
        Context appContext = InstrumentationRegistry.getTargetContext();
        HelperFactory.setHelper(appContext);

        // Create repository
        repository = new TimeMetricsRepository();
        aspectsRepository = new AspectsRepository();

        // Create aspect_1
        aspect_1 = new Aspect();
        buildAspect(aspect_1, 1);
        aspect_2 = new Aspect();
        buildAspect(aspect_2, 2);

        // Create test metrics
        activeTimeMetric = new TimeMetric();
        buildTestMetric(activeTimeMetric, aspect_1,1, false, true);
        notActiveTimeMetric = new TimeMetric();
        buildTestMetric(notActiveTimeMetric, aspect_1,2, false, false);
        activeIntervalMetric = new TimeMetric();
        buildTestMetric(activeIntervalMetric, aspect_2,3, true, true);
        notActiveIntervalMetric = new TimeMetric();
        buildTestMetric(notActiveIntervalMetric, aspect_2,4, true, false);

        testMetricData_1 = new TimeMetricData();
        buildTestMetricData(testMetricData_1, 1, activeTimeMetric);
        testMetricData_2 = new TimeMetricData();
        buildTestMetricData(testMetricData_2, 2, notActiveTimeMetric);

        flushRepository();
        flushAspectRepository();
        aspectsRepository.addOrUpdateAspect(aspect_1);
        aspectsRepository.addOrUpdateAspect(aspect_2);
    }

    @Test
    public void timeSpecificTest() {
        // Arrange
        Calendar.Builder calendarBuilder = new Calendar.Builder();
        calendarBuilder.setDate(2018, 12, 12);
        calendarBuilder.setTimeOfDay(20, 0, 0);
        Calendar calendar = calendarBuilder.build();

        Date date = calendar.getTime();

        testMetricData_1.setDate(date);
        repository.addOrUpdateMetric(activeTimeMetric);

        // Act
        repository.addMetricData(activeTimeMetric, testMetricData_1);

        // Assert
        List<TimeMetricData> data = repository.getAllDataForMetrics(activeTimeMetric);
        assertEquals(1, data.size());
        assertMetricsData(testMetricData_1, data.get(0));

        Date actualDate = data.get(0).getDate();
        Calendar.Builder newBuilder = new Calendar.Builder();
        newBuilder.setInstant(actualDate);
        Calendar newCalendar = newBuilder.build();

        assertEquals(calendar.getTime(), newCalendar.getTime());
        assertEquals(calendar.getTimeInMillis(), newCalendar.getTimeInMillis());
        assertEquals(calendar.get(Calendar.DAY_OF_MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        assertEquals(calendar.get(Calendar.MONTH), newCalendar.get(Calendar.MONTH));
        assertEquals(calendar.get(Calendar.YEAR), newCalendar.get(Calendar.YEAR));
        assertEquals(calendar.get(Calendar.DAY_OF_YEAR), newCalendar.get(Calendar.DAY_OF_YEAR));
        assertEquals(calendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.HOUR_OF_DAY));
        assertEquals(calendar.get(Calendar.MINUTE), newCalendar.get(Calendar.MINUTE));
        assertEquals(calendar.get(Calendar.SECOND), newCalendar.get(Calendar.SECOND));
    }

    @Test
    public void getAllTimeIntervalMetricsTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<TimeMetric> metrics = repository.getTimeIntervalMetrics(false);

        // Assert
        assertEquals(2, metrics.size());
        assertMetrics(activeIntervalMetric, metrics.get(0));
        assertMetrics(notActiveIntervalMetric, metrics.get(1));
    }

    @Test
    public void getActiveTimeIntervalMetricsTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<TimeMetric> metrics = repository.getTimeIntervalMetrics(true);

        // Assert
        assertEquals(1, metrics.size());
        assertMetrics(activeIntervalMetric, metrics.get(0));
    }

    @Test
    public void getAllTimeSimpleMetricsTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<TimeMetric> metrics = repository.getTimeSimpleMetrics(false);

        // Assert
        assertEquals(2, metrics.size());
        assertMetrics(activeTimeMetric, metrics.get(0));
        assertMetrics(notActiveTimeMetric, metrics.get(1));
    }

    @Test
    public void getActiveTimeSimpleMetricsTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<TimeMetric> metrics = repository.getTimeSimpleMetrics(true);

        // Assert
        assertEquals(1, metrics.size());
        assertMetrics(activeTimeMetric, metrics.get(0));
    }

    @Test
    public void getAllMetricsTest() {
        // Arrange
        addAllMetrics();

        // Act
        List<TimeMetric> metrics = repository.getMetrics(false);

        // Assert
        assertEquals(4, metrics.size());
        assertMetrics(activeTimeMetric, metrics.get(0));
        assertMetrics(notActiveTimeMetric, metrics.get(1));
        assertMetrics(activeIntervalMetric, metrics.get(2));
        assertMetrics(notActiveIntervalMetric, metrics.get(3));
    }

    @Test
    public void getActiveMetricsTest() {
        // Arrange
        addAllMetrics();

        // Act
        List<TimeMetric> metrics = repository.getMetrics(true);

        // Assert
        assertEquals(2, metrics.size());
        assertMetrics(activeTimeMetric, metrics.get(0));
        assertMetrics(activeIntervalMetric, metrics.get(1));
    }

    @Test
    public void getAllMetricsForAspectTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<TimeMetric> metrics = repository.getMetricsForAspect(aspect_1, false);

        // Assert
        assertEquals(2, metrics.size());
        assertMetrics(activeTimeMetric, metrics.get(0));
        assertMetrics(notActiveTimeMetric, metrics.get(1));
    }

    @Test
    public void getActiveMetricsForAspectTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<TimeMetric> timeMetrics = repository.getMetricsForAspect(aspect_1, true);
        List<TimeMetric> intervalMetrics = repository.getMetricsForAspect(aspect_2, true);

        // Assert
        assertEquals(1, timeMetrics.size());
        assertMetrics(activeTimeMetric, timeMetrics.get(0));
        assertEquals(1, intervalMetrics.size());
        assertMetrics(activeIntervalMetric, intervalMetrics.get(0));
    }

    @Test
    public void addMetricTest() {
        // Act
        repository.addOrUpdateMetric(activeTimeMetric);

        // Assert
        List<TimeMetric> listOfMetric = repository.getMetrics(false);
        assertEquals(1, listOfMetric.size());
        assertMetrics(activeTimeMetric, listOfMetric.get(0));
    }

    @Test
    public void updateMetricTest() {
        // Arrange
        List<TimeMetric> listOfMetric;
        repository.addOrUpdateMetric(activeTimeMetric);

        activeTimeMetric.setName(notActiveTimeMetric.getName());
        activeTimeMetric.setActive(notActiveTimeMetric.isActive());
        activeTimeMetric.setQuestion(notActiveTimeMetric.getQuestion());
        activeTimeMetric.setAspect(notActiveTimeMetric.getAspect());

        // Act
        repository.addOrUpdateMetric(activeTimeMetric);

        // Assert
        listOfMetric = repository.getMetrics(false);
        assertEquals(1, listOfMetric.size());

        TimeMetric actualMetric = listOfMetric.get(0);
        assertMetrics(activeTimeMetric, actualMetric);
    }

    @Test
    public void findMetricsByNameTest() {
        // Arrange
        addAllMetrics();

        // Act
        TimeMetric metricFound = repository.findMetricsByName(activeTimeMetric.getName());

        // Assert
        assertMetrics(activeTimeMetric, metricFound);
    }

    @Test
    public void findMetricsByNameNotFoundTest() {
        // Act
        TimeMetric metricFound = repository.findMetricsByName(notActiveTimeMetric.getName());

        // Assert
        assertNull(metricFound);
    }

    @Test
    public void removeMetricTest() {
        // Arrange
        addAllMetrics();

        // Act
        repository.removeMetric(notActiveTimeMetric);

        // Assert
        TimeMetric actualMetric = repository.findMetricsByName(activeTimeMetric.getName());
        assertMetrics(activeTimeMetric, actualMetric);
    }

    @Test
    public void removeMetricEmptyTest() {

        // Act
        repository.removeMetric(activeTimeMetric);

        // Assert
        assertEquals( 0, repository.getMetrics(false).size());
    }

    @Test
    public void addMetricDataTest() {
        // Arrange
        repository.addOrUpdateMetric(activeTimeMetric);

        // Act
        TimeMetricData result = repository.addMetricData(activeTimeMetric, testMetricData_1);

        // Assert
        assertMetricsData(testMetricData_1, result);

        List<TimeMetricData> resList = repository.getAllDataForMetrics(activeTimeMetric);
        assertMetricsData(testMetricData_1, resList.get(0));
        assertEquals(1, resList.size());
    }

    @Test
    public void addMetricDataNullMetricTest() {
        // Act
        TimeMetricData result = repository.addMetricData(null, testMetricData_1);

        // Assert
        assertNull(result);
    }

    @Test
    public void updateMetricDataTest() {
        // Arrange
        addMetricWithData();

        Date newDate = new Date();
        newDate.setTime(1000);
        testMetricData_1.setDate(newDate);

        // Act
        repository.updateMetricData(testMetricData_1);

        // Assert
        List<TimeMetricData> resList = repository.getAllDataForMetrics(activeTimeMetric);
        assertMetricsData(testMetricData_1, resList.get(0));
    }

    @Test
    public void updateMetricDataEmptyTest() {
        // Arrange
        repository.addOrUpdateMetric(activeTimeMetric);
        repository.addMetricData(activeTimeMetric, testMetricData_2);

        // Act
        repository.updateMetricData(testMetricData_1);

        // Assert
        List<TimeMetricData> resList = repository.getAllDataForMetrics(activeTimeMetric);
        assertEquals(1, resList.size());
        assertMetricsData(testMetricData_2, resList.get(0));
    }

    @Test
    public void removeMetricDataTest() {
        // Arrange
        addMetricWithData();

        // Act
        repository.removeMetricData(testMetricData_1);

        // Assert
        List<TimeMetricData> resList = repository.getAllDataForMetrics(activeTimeMetric);
        assertMetricsData(testMetricData_2, resList.get(0));
        assertEquals(1, resList.size());
    }

    @Test
    public void getAllDataForMetricsTest() {
        // Arrange
        addMetricWithData();

        // Act
        List<TimeMetricData> metricsData = repository.getAllDataForMetrics(activeTimeMetric);

        // Assert
        assertEquals(2, metricsData.size());
        assertMetricsData(testMetricData_1, metricsData.get(0));
        assertMetricsData(testMetricData_2, metricsData.get(1));
    }

    @Test
    public void getDataForMetricsTest() throws SQLException {
        // Arrange
        TimeMetricData testMetricData_3 = new TimeMetricData();
        buildTestMetricData(testMetricData_3, 3, activeTimeMetric);

        TimeMetricData testMetricData_4 = new TimeMetricData();
        buildTestMetricData(testMetricData_4, 4, activeTimeMetric);

        addMetricWithData();
        repository.addMetricData(activeTimeMetric, testMetricData_3);
        repository.addMetricData(activeTimeMetric, testMetricData_4);

        // Act
        List<TimeMetricData> resList = repository.getDataForMetrics(activeTimeMetric, testMetricData_2.getDate(), testMetricData_3.getDate());

        // Assert
        assertEquals(2, resList.size());
        assertMetricsData(testMetricData_2, resList.get(0));
        assertMetricsData(testMetricData_3, resList.get(1));
    }

    @Test
    public void getDataForMetricsErrorTest() {
        // Arrange
        boolean isFailed = false;

        // Act
        try {
            repository.getDataForMetrics(null, testMetricData_2.getDate(), testMetricData_1.getDate());
        }
        catch (SQLException exception) {
            isFailed = true;
        }

        // Assert
        assertTrue(isFailed);
    }

    @Test
    public void getDataForMetricsEmptyTest() throws SQLException {
        // Arrange
        addMetricWithData();

        // Act
        List<TimeMetricData> resList = repository.getDataForMetrics(activeTimeMetric, testMetricData_2.getDate(), testMetricData_1.getDate());

        // Assert
        assertTrue(resList.isEmpty());
    }

    @Test
    public void removeAllMetricsDataTest() throws SQLException {
        // Arrange
        addMetricWithData();

        // Act
        repository.removeAllMetricsData(activeTimeMetric);


        List<TimeMetricData> metricsData = repository.getAllDataForMetrics(activeTimeMetric);

        // Assert
        assertEquals(0, metricsData.size());
    }

    @After
    public void releaseAll() {
        HelperFactory.releaseHelper();

        repository = null;
        aspect_1 = null;
        aspectsRepository = null;
        activeTimeMetric = null;
        notActiveTimeMetric = null;
        testMetricData_1 = null;
        testMetricData_2 = null;
    }


    private void buildAspect(Aspect aspect, int i) {
        String index = Integer.toString(i);

        aspect.setActive(true);
        aspect.setName("test_aspect_" + index);
    }

    /** Sets test TextMetric by test values */
    private void buildTestMetric(TimeMetric metric, Aspect aspect, int i, boolean isInterval, boolean isActive) {
        String index = Integer.toString(i);

        metric.setName("test_metric_name_" + index);
        metric.setQuestion("question_" + index);
        metric.setActive(isActive);
        metric.setInterval(isInterval);
        metric.setAspect(aspect);
    }

    /** Sets test TextMetricData by test values */
    private void buildTestMetricData(TimeMetricData data, int i, TimeMetric metric) {
        Date date = new Date(i*10000);
        Date startDate = new Date(i*20000);
        Date endDate = new Date(i*30000);

        data.setDate(date);
        data.setStartTime(startDate);
        data.setEndTime(endDate);
        data.setMetric(metric);
    }

    private void assertAspects(Aspect expected, Aspect actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.isActive(), actual.isActive());
    }

    /** Asserts expected and actual text metrics */
    private void assertMetrics(TimeMetric expected, TimeMetric actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.isInterval(), actual.isInterval());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getQuestion(), actual.getQuestion());
        assertEquals(expected.isActive(), actual.isActive());
        assertAspects(expected.getAspect(), actual.getAspect());
    }

    /** Asserts expected and actual text metrics data*/
    private void assertMetricsData(TimeMetricData expected, TimeMetricData actual) {
        assertEquals(expected.getDate(), actual.getDate());
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getStartTime(), actual.getStartTime());
        assertEquals(expected.getEndTime(), actual.getEndTime());
        assertMetrics(expected.getMetric(), actual.getMetric());
    }

    private void flushRepository() throws SQLException {
        List<TimeMetric> listOfMetric = repository.getMetrics(false);
        for (int i = 0; i < listOfMetric.size(); i++) {
            repository.removeAllMetricsData(listOfMetric.get(i));
            repository.removeMetric(listOfMetric.get(i));
        }
    }

    private void flushAspectRepository() {
        List<Aspect> aspects = aspectsRepository.getAspects(false);
        for (int i = 0; i < aspects.size(); i++) {
            aspectsRepository.removeAspect(aspects.get(i));
        }
    }

    private void addAllMetrics(){
        repository.addOrUpdateMetric(activeTimeMetric);
        repository.addOrUpdateMetric(notActiveTimeMetric);
        repository.addOrUpdateMetric(activeIntervalMetric);
        repository.addOrUpdateMetric(notActiveIntervalMetric);
    }

    private void addMetricWithData(){
        repository.addOrUpdateMetric(activeTimeMetric);
        repository.addMetricData(activeTimeMetric, testMetricData_1);
        repository.addMetricData(activeTimeMetric, testMetricData_2);
    }

}
