package com.gss.headwaytracker;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.gss.database.HelperFactory;
import com.gss.database.model.Aspect;
import com.gss.database.model.TextMetric;
import com.gss.database.model.TextMetricData;
import com.gss.database.repositories.implementation.AspectsRepository;
import com.gss.database.repositories.implementation.TextMetricsRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class TextMetricsRepositoryTest {
    private TextMetricsRepository repository;
    private AspectsRepository aspectsRepository;

    private Aspect aspect_1;
    private Aspect aspect_2;

    // Text metrics
    private TextMetric activeTextMetric;
    private TextMetric notActiveTextMetric;
    private TextMetric otherActiveTextMetric;

    // Text metric data
    private TextMetricData testMetricData_1;
    private TextMetricData testMetricData_2;

    @Before
    public void setUp() throws SQLException {
        Context appContext = InstrumentationRegistry.getTargetContext();
        HelperFactory.setHelper(appContext);

        // Create repository
        repository = new TextMetricsRepository();
        aspectsRepository = new AspectsRepository();

        // Create aspect_1
        aspect_1 = new Aspect();
        buildAspect(aspect_1, 1);
        aspect_2 = new Aspect();
        buildAspect(aspect_2, 2);

        // Create test metrics
        activeTextMetric = new TextMetric();
        buildTestTextMetric(activeTextMetric, aspect_1, 1, true);
        notActiveTextMetric = new TextMetric();
        buildTestTextMetric(notActiveTextMetric, aspect_1, 2, false);
        otherActiveTextMetric = new TextMetric();
        buildTestTextMetric(otherActiveTextMetric, aspect_2, 3, true);


        testMetricData_1 = new TextMetricData();
        buildTestTextMetricData(testMetricData_1, 1, activeTextMetric);
        testMetricData_2 = new TextMetricData();
        buildTestTextMetricData(testMetricData_2, 2, notActiveTextMetric);

        flushRepository();
        flushAspectRepository();
        aspectsRepository.addOrUpdateAspect(aspect_1);
        aspectsRepository.addOrUpdateAspect(aspect_2);
    }

    @Test
    public void getAllMetricsTest() {
        // Arrange
        addAllMetrics();

        // Act
        List<TextMetric> metrics = repository.getMetrics(false);

        // Assert
        assertEquals(3, metrics.size());
        assertMetrics(activeTextMetric, metrics.get(0));
        assertMetrics(notActiveTextMetric, metrics.get(1));
        assertMetrics(otherActiveTextMetric, metrics.get(2));

    }

    @Test
    public void getActiveMetricsTest() {
        // Arrange
        addAllMetrics();

        // Act
        List<TextMetric> metrics = repository.getMetrics(true);

        // Assert
        assertEquals(2, metrics.size());
        assertMetrics(activeTextMetric, metrics.get(0));
        assertMetrics(otherActiveTextMetric, metrics.get(1));
    }

    @Test
    public void getAllMetricsForAspectTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<TextMetric> metrics = repository.getMetricsForAspect(aspect_1, false);

        // Assert
        assertEquals(2, metrics.size());
        assertMetrics(activeTextMetric, metrics.get(0));
        assertMetrics(notActiveTextMetric, metrics.get(1));
    }


    @Test
    public void getActiveMetricsForAspectTest() throws SQLException {
        // Arrange
        addAllMetrics();

        // Act
        List<TextMetric> metrics = repository.getMetricsForAspect(aspect_1, true);

        // Assert
        assertEquals(1, metrics.size());
        assertMetrics(activeTextMetric, metrics.get(0));
    }

    @Test
    public void addMetricTest() {
        // Arrange
        List<TextMetric> listOfMetric;

        // Act
        repository.addOrUpdateMetric(activeTextMetric);

        // Assert
        listOfMetric = repository.getMetrics(false);
        assertEquals(1, listOfMetric.size());

        TextMetric actualMetric = listOfMetric.get(0);
        assertMetrics(activeTextMetric, actualMetric);
    }

    @Test
    public void updateMetricTest() {
        // Arrange
        List<TextMetric> listOfMetric;
        repository.addOrUpdateMetric(activeTextMetric);

        activeTextMetric.setName(notActiveTextMetric.getName());
        activeTextMetric.setActive(notActiveTextMetric.isActive());
        activeTextMetric.setQuestion(notActiveTextMetric.getQuestion());
        activeTextMetric.setAnswerCount(notActiveTextMetric.getAnswerCount());
        activeTextMetric.setAspect(notActiveTextMetric.getAspect());

        // Act
        repository.addOrUpdateMetric(activeTextMetric);

        // Assert
        listOfMetric = repository.getMetrics(false);
        assertEquals(1, listOfMetric.size());

        TextMetric actualMetric = listOfMetric.get(0);
        assertMetrics(activeTextMetric, actualMetric);
    }

    @Test
    public void findMetricsByNameTest() {
        // Arrange
        addAllMetrics();

        // Act
        TextMetric metricFound = repository.findMetricsByName(activeTextMetric.getName());

        // Assert
        assertMetrics(activeTextMetric, metricFound);
    }

    @Test
    public void findMetricsByNameNotFoundTest() {
        // Act
        TextMetric metricFound = repository.findMetricsByName(notActiveTextMetric.getName());

        // Assert
        assertNull(metricFound);
    }

    @Test
    public void removeMetricTest() {
        // Arrange
        addAllMetrics();

        // Act
        repository.removeMetric(notActiveTextMetric);

        // Assert
        TextMetric actualMetric = repository.findMetricsByName(activeTextMetric.getName());
        assertMetrics(activeTextMetric, actualMetric);
    }

    @Test
    public void removeMetricEmptyTest() {
        // Act
        repository.removeMetric(activeTextMetric);

        // Assert
        assertEquals( 0, repository.getMetrics(false).size());
    }

    @Test
    public void addMetricDataTest() {
        // Arrange
        repository.addOrUpdateMetric(activeTextMetric);

        // Act
        TextMetricData result = repository.addMetricData(activeTextMetric, testMetricData_1);

        // Assert
        assertMetricsData(testMetricData_1, result);

        List<TextMetricData> resList = repository.getAllDataForMetrics(activeTextMetric);
        assertMetricsData(testMetricData_1, resList.get(0));
        assertEquals(1, resList.size());
    }

    @Test
    public void addMetricDataNullMetricTest() {
        // Act
        TextMetricData result = repository.addMetricData(null, testMetricData_1);

        // Assert
        assertNull(result);
    }

    @Test
    public void updateMetricDataTest() {
        // Arrange
        repository.addOrUpdateMetric(activeTextMetric);
        repository.addMetricData(activeTextMetric, testMetricData_1);

        testMetricData_1.setValueString("new value");
        Date newDate = new Date();
        newDate.setTime(1000);
        testMetricData_1.setDate(newDate);

        // Act
        repository.updateMetricData(testMetricData_1);

        // Assert
        List<TextMetricData> resList = repository.getAllDataForMetrics(activeTextMetric);
        assertEquals(1, resList.size());
        assertMetricsData(testMetricData_1, resList.get(0));
    }

    @Test
    public void updateMetricDataEmptyTest() {
        // Arrange
        repository.addOrUpdateMetric(activeTextMetric);
        repository.addMetricData(activeTextMetric, testMetricData_2);

        // Act
        repository.updateMetricData(testMetricData_1);

        // Assert
        List<TextMetricData> resList = repository.getAllDataForMetrics(activeTextMetric);
        assertEquals(1, resList.size());
        assertMetricsData(testMetricData_2, resList.get(0));
    }

    @Test
    public void removeMetricDataTest() {
        // Arrange
        addMetricsWithData();

        // Act
        repository.removeMetricData(testMetricData_1);

        // Assert
        List<TextMetricData> resList = repository.getAllDataForMetrics(activeTextMetric);
        assertMetricsData(testMetricData_2, resList.get(0));
        assertEquals(1, resList.size());
    }

    @Test
    public void getAllDataForMetricsTest() {
        // Arrange
        addMetricsWithData();

        // Act
        List<TextMetricData> metricsData = repository.getAllDataForMetrics(activeTextMetric);

        // Assert
        assertEquals(2, metricsData.size());
        assertMetricsData(testMetricData_1, metricsData.get(0));
        assertMetricsData(testMetricData_2, metricsData.get(1));
    }

    @Test
    public void getDataForMetricsTest() throws SQLException {
        // Arrange
        TextMetricData testMetricData_3 = new TextMetricData();
        buildTestTextMetricData(testMetricData_3, 3, activeTextMetric);

        TextMetricData testMetricData_4 = new TextMetricData();
        buildTestTextMetricData(testMetricData_4, 4, activeTextMetric);

        addMetricsWithData();
        repository.addMetricData(activeTextMetric, testMetricData_3);
        repository.addMetricData(activeTextMetric, testMetricData_4);

        // Act
        List<TextMetricData>  resList = repository.getDataForMetrics(activeTextMetric, testMetricData_2.getDate(), testMetricData_3.getDate());

        // Assert
        assertEquals(2, resList.size());
        assertMetricsData(testMetricData_2, resList.get(0));
        assertMetricsData(testMetricData_3, resList.get(1));
    }

    @Test
    public void getDataForMetricsErrorTest() {
        // Arrange
        boolean isFailed = false;

        // Act
        try {
            repository.getDataForMetrics(null, testMetricData_2.getDate(), testMetricData_1.getDate());
        }
        catch (SQLException exception) {
            isFailed = true;
        }

        // Assert
        assertTrue(isFailed);
    }

    @Test
    public void getDataForMetricsEmptyTest() throws SQLException {
        // Arrange
        addMetricsWithData();

        // Act
        List<TextMetricData>  resList = repository.getDataForMetrics(activeTextMetric, testMetricData_2.getDate(), testMetricData_1.getDate());

        // Assert
        assertTrue(resList.isEmpty());
    }

    @Test
    public void removeAllMetricsDataTest() throws SQLException {
        // Arrange
        addMetricsWithData();

        // Act
        repository.removeAllMetricsData(activeTextMetric);

        List<TextMetricData> metricsData = repository.getAllDataForMetrics(activeTextMetric);

        // Assert
        assertEquals(0, metricsData.size());
    }

    @After
    public void releaseAll() {
        HelperFactory.releaseHelper();

        repository = null;
        aspect_1 = null;
        aspectsRepository = null;
        activeTextMetric = null;
        notActiveTextMetric = null;
        testMetricData_1 = null;
        testMetricData_2 = null;
    }


    private void buildAspect(Aspect aspect, int i) {
        String index = Integer.toString(i);

        aspect.setActive(true);
        aspect.setName("test_aspect_" + index);
    }

    /** Sets test TextMetric by test values */
    private void buildTestTextMetric(TextMetric metric, Aspect aspect, int i, boolean isActive) {
        String index = Integer.toString(i);

        metric.setName("test_metric_name_" + index);
        metric.setQuestion("question_" + index);
        metric.setActive(isActive);
        metric.setAnswerCount(1);
        metric.setAspect(aspect);
    }

    /** Sets test TextMetricData by test values */
    private void buildTestTextMetricData(TextMetricData data, int i, TextMetric metric) {
        String index = Integer.toString(i);

        Date date = new Date();
        date.setTime(i);

        data.setDate(date);
        data.setValueString("test_value_data_" + index);
        data.setMetric(metric);
    }

    private void assertAspects(Aspect expected, Aspect actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.isActive(), actual.isActive());
    }

    /** Asserts expected and actual text metrics */
    private void assertMetrics(TextMetric expected, TextMetric actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getAnswerCount(), actual.getAnswerCount());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getQuestion(), actual.getQuestion());
        assertEquals(expected.isActive(), actual.isActive());
        assertAspects(expected.getAspect(), actual.getAspect());
    }

    /** Asserts expected and actual text metrics data*/
    private void assertMetricsData(TextMetricData expected, TextMetricData actual) {
        assertEquals(expected.getDate(), actual.getDate());
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getValue(), actual.getValue());
        assertMetrics(expected.getMetric(), actual.getMetric());
    }

    private void flushRepository() throws SQLException {
        List<TextMetric> listOfMetric = repository.getMetrics(false);
        for (int i = 0; i < listOfMetric.size(); i++) {
            repository.removeAllMetricsData(listOfMetric.get(i));
            repository.removeMetric(listOfMetric.get(i));
        }
    }

    private void flushAspectRepository() {
        List<Aspect> aspects = aspectsRepository.getAspects(false);
        for (int i = 0; i < aspects.size(); i++) {
            aspectsRepository.removeAspect(aspects.get(i));
        }
    }

    private void addAllMetrics(){
        repository.addOrUpdateMetric(activeTextMetric);
        repository.addOrUpdateMetric(notActiveTextMetric);
        repository.addOrUpdateMetric(otherActiveTextMetric);
    }

    private void addMetricsWithData(){
        repository.addOrUpdateMetric(activeTextMetric);
        repository.addMetricData(activeTextMetric, testMetricData_1);
        repository.addMetricData(activeTextMetric, testMetricData_2);
    }
}
